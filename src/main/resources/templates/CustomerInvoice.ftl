<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
<title>Customer Email</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />


<style type="text/css">
body {
	font-family: 'Roboto', sans-serif;
	font-size: 16px;
}

</style>
</head>
<body >
	<div
		style="width: 1170px;">
		<h5>Dear ${address.name?cap_first},</h5>
		<div>
			<p><b>Invoice Generated.</b></p>
			<p>I hope you’re well! Please see attached invoice for order - ${id}. Don’t hesitate to reach out if you have any questions.</p>			
		</div>
		
		
		
		<h5>
			Thanks,<br /> Kaiya(<a href="tel:+91 9566144275">
              Call : +91 9566144275
            </a>).
		</h5>
	</div>
</body>

</html>