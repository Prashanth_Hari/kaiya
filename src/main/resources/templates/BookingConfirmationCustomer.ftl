<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
<title>Customer Email</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />


<style type="text/css">
body {
	font-family: 'Roboto', sans-serif;
	font-size: 16px;
}

</style>
</head>
<body >
	<div
		style="width: 1170px;">
		<h5>Dear ${address.name?cap_first},</h5>
		<div>
			<p><b>Order Confirmation</b></p>
			<p>We have received your order(${id})and you can find your purchase information below</p>				
		</div>
		
		<div style="margin-bottom: 20px;">
			<div style="margin-bottom: 0; border-radius: 4px; background-color: #fff; border: 1px solid #f55d2c;">
				<div
					style="border-bottom: 0; color: #fff; background-color: #f55d2c; border-color: #f55d2c; padding: 10px 15px; border-top-left-radius: 3px; border-top-right-radius: 3px;">BookingDetails</div>
				<div style="padding: 15px;">
					<table style="border-collapse: collapse; width: 100%; font-size: 16px;">
						<thead style="border-bottom: 1px solid #d4d1d1;">
							<tr
								style="padding: 15px; text-align: left; border-collapse: collapse;">
								<th
									style="padding: 15px; text-align: left; border-collapse: collapse;">Items</th>
								<th
									style="padding: 15px; text-align: left; border-collapse: collapse;">Qty</th>
								<th
									style="padding: 15px; text-align: left; border-collapse: collapse;">Price</th>
								<th
									style="padding: 15px; text-align: left; border-collapse: collapse;">Order Date</th>
								<th
									style="padding: 15px; text-align: left; border-collapse: collapse;">Status</th>
								
								<th
									style="padding: 15px; text-align: left; border-collapse: collapse;"></th>
							</tr>
						</thead>
						<tbody>
							<#list orderDetails as orderDetails>
							<tr 
								style="padding: 15px; text-align: left; border-collapse: collapse;">
							
	
								<td 
									style="padding: 15px; text-align: left; border-collapse: collapse;">${orderDetails.productName}-${orderDetails.litres}</td>						
								
										
								<td
									style="padding: 15px; text-align: left; border-collapse: collapse;"> ${orderDetails.quantity}</td>
								<td
									style="padding: 15px; text-align: left; border-collapse: collapse;"> ${orderDetails.totalAmount}</td>
								<td
									style="padding: 15px; text-align: left; border-collapse: collapse;">${orderDetails.createdDate?date('yyyy-MM-dd')?string["dd/MM/yyyy"]}</td>
									<td
									style="padding: 15px; text-align: left; border-collapse: collapse;">${comments}</td>
									
								
							</tr>
							</#list>
						</tbody>
					</table>
					<div style="padding: 15px; text-align: left;">CGST(9%):Rs ${orderAmount*9/100}</div>
					<div style="padding: 15px; text-align: left;">SGST(9%):Rs ${orderAmount*9/100}</div>
					<div style="padding: 15px; text-align: left;">IGST(0%):Rs 0</div>
					<div style="padding: 15px; text-align: left;">Total Amount:Rs ${totalOrderAmount}</div>
				</div>
			</div>
		</div>
		<h5>
			Thanks,<br /> Kaiya(<a href="tel:+91 9566144275">
              Call : +91 9566144275
            </a>).
		</h5>
	</div>
</body>

</html>