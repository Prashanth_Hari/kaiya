<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Booking Confirmed</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<link href='http://fonts.googleapis.com/css?family=Roboto'
	rel='stylesheet' type='text/css' />

<!-- use the font -->
<style>
body {
	font-family: 'Roboto', sans-serif;
	font-size: 28px;
}
</style>
</head>
<body>
	<h4>Dear Admin,</h4>

	<p>Our Customer has raised a query. Please find the below details.</p>
	<p>
		<b>Name:</b> ${name}
		
	</p>
	<p>
	<b>Description:</b> ${message}
		</p>
	<h4>
		Thanks,<br/>
		${name}.
	</h4>
	
</body>
</html>