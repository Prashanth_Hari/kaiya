<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />


<style type="text/css">
body {
	font-family: 'Roboto', sans-serif;
	font-size: 18px;
}
 
</style>
</head>
<body >
	<div>
		<h3>Dear Customer ,</h3>
		<div>
			
		<p>To authenticate, please use the following One Time Password (OTP):</p><br/>
		${otp}
	<p>Don't share this OTP with anyone.</p>

	<p>We hope to see you again soon.</p>

</div>		
			Thanks,
			<br/> Kaiya
	</div>
</body>

</html>