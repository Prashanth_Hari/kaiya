package com.kaiya.exceptions;

public class ProductDetailException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ProductDetailException(String exceptionMessage) {
		super(exceptionMessage);
	}
}
