package com.kaiya.exceptions;

public class CustomerDetailException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CustomerDetailException(String exceptionMessage) {
		super(exceptionMessage);
	}
}
