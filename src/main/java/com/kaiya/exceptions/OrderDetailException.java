package com.kaiya.exceptions;

public class OrderDetailException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public OrderDetailException(String exceptionMessage) {
		super(exceptionMessage);
	}
}
