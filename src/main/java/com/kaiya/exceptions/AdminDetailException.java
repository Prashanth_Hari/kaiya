package com.kaiya.exceptions;

public class AdminDetailException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AdminDetailException(String exceptionMessage) {
		super(exceptionMessage);
	}
}
