package com.kaiya.exceptions;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.kaiya.entity.ErrorResponse;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(AdminDetailException.class)
	public final ResponseEntity<Object> adminNotFound(final AdminDetailException exception,
			final HttpServletRequest request) {
		ErrorResponse error = new ErrorResponse();
		error.setCode(102);
		error.setErrorMessage(exception.getMessage());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(CustomerDetailException.class)
	public final ResponseEntity<Object> customerNotFound(final CustomerDetailException exception,
			final HttpServletRequest request) {
		ErrorResponse error = new ErrorResponse();
		error.setCode(102);
		error.setErrorMessage(exception.getMessage());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(InvalidCredentialException.class)
	public final ResponseEntity<Object> invalidCredential(final InvalidCredentialException exception,
			final HttpServletRequest request) {
		ErrorResponse error = new ErrorResponse();
		error.setCode(100);
		error.setErrorMessage(exception.getMessage());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ProductDetailException.class)
	public final ResponseEntity<Object> productException(final ProductDetailException exception,
			final HttpServletRequest request) {
		ErrorResponse error = new ErrorResponse();
		error.setCode(104);
		error.setErrorMessage(exception.getMessage());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(OrderDetailException.class)
	public final ResponseEntity<Object> orderNotFound(final OrderDetailException exception,
			final HttpServletRequest request) {
		ErrorResponse error = new ErrorResponse();
		error.setCode(105);
		error.setErrorMessage(exception.getMessage());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(InvoiceDetailException.class)
	public final ResponseEntity<Object> invoiceDetailNotFound(final InvoiceDetailException exception,
			final HttpServletRequest request) {
		ErrorResponse error = new ErrorResponse();
		error.setCode(106);
		error.setErrorMessage(exception.getMessage());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
