package com.kaiya.exceptions;

public class InvoiceDetailException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvoiceDetailException(String exceptionMessage) {
		super(exceptionMessage);
	}
}
