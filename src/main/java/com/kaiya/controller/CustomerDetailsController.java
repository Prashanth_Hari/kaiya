package com.kaiya.controller;

import static com.kaiya.api.Constants.CHANGE_PASSWORD;
import static com.kaiya.api.Constants.RESET_PASSWORD;
import static com.kaiya.api.Constants.RESET_PASSWORD_EMAIL_LINK;
import static com.kaiya.api.Constants.CUSTOMER_DETAILS_REQUIRED;
import static com.kaiya.api.Constants.GET_ALL_ADMIN;
import static com.kaiya.api.Constants.GET_CUSTOMER;
import static com.kaiya.api.Constants.CREATE_CUSTOMER;
import static com.kaiya.api.Constants.UPDATE_CUSTOMER;
import static com.kaiya.api.Constants.GET_CUSTOMER_DETAILS_BY_ID;
import static com.kaiya.api.Constants.VALIDATE_OTP;
import static com.kaiya.api.Constants.GET_ALL_CUSTOMER;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kaiya.entity.CustomerDetails;
import com.kaiya.exceptions.CustomerDetailException;
import com.kaiya.model.Changepassword;
import com.kaiya.model.ResetPassword;
import com.kaiya.service.CustomerService;
import com.kaiya.service.ProductService;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin(origins = "*")
@Api(tags = "Customer Details", description = "Customer Api's")
public class CustomerDetailsController {

	private static Logger LOGGER = LogManager.getLogger(CustomerDetailsController.class);

	@Autowired
	CustomerService customerService;

	@Autowired
	ProductService productService;

	@PostMapping(CREATE_CUSTOMER)
	private ResponseEntity<?> saveCustomer(@RequestBody CustomerDetails customerDetails) {

		LOGGER.info("Inside Create Customer");
		if (customerDetails == null) {
			throw new CustomerDetailException(CUSTOMER_DETAILS_REQUIRED);
		}

		return customerService.saveCustomer(customerDetails);

	}

	@PutMapping(UPDATE_CUSTOMER)
	private ResponseEntity<?> updateCustomerDetails(@RequestBody CustomerDetails customerDetails) {
		LOGGER.info("Inside Update Customer");

		return customerService.updateCustomerDetails(customerDetails);

	}

	@PostMapping(CHANGE_PASSWORD)
	private ResponseEntity<?> changePassword(@RequestBody Changepassword changePassword) {
		LOGGER.info("Inside Change Password");

		return customerService.changePassword(changePassword);

	}

	@PostMapping(RESET_PASSWORD)
	private ResponseEntity<?> resetPassword(@RequestBody ResetPassword resetPassword) {
		LOGGER.info("Inside Reset Password");

		return customerService.resetPassword(resetPassword);

	}

	@PostMapping(RESET_PASSWORD_EMAIL_LINK)
	private ResponseEntity<?> sendResetPasswordOtp(@RequestBody CustomerDetails resetPassword) {
		LOGGER.info("Inside Email Service for Forgot password");

		return customerService.sendResetPasswordOtp(resetPassword);

	}

	@PostMapping(VALIDATE_OTP)
	private ResponseEntity<?> validateOTP(@RequestBody CustomerDetails customerDetails) {
		LOGGER.info("Inside Validate OTP");

		return customerService.validateOTP(customerDetails);

	}

	@GetMapping(GET_CUSTOMER + "/{emailId}")
	public CustomerDetails getCustomerDetails(@PathVariable("emailId") String emailId) {
		LOGGER.info("Inside get customer details by emailid");

		return customerService.validateLogin(emailId);
	}

	@GetMapping(GET_ALL_CUSTOMER)
	public ResponseEntity<?> getAllCustomerDetails() {
		LOGGER.info("Inside get all customer details");

		return customerService.getAllCustomerDetails();
	}

	@GetMapping(GET_CUSTOMER_DETAILS_BY_ID + "/{customerId}")
	public CustomerDetails getCustomerDetailsById(@PathVariable("customerId") String customerId) {

		LOGGER.info("Inside get customer details by customerId");

		return customerService.getCustomerDetails(customerId);
	}
}
