package com.kaiya.controller;

import static com.kaiya.api.Constants.CHANGE_PASSWORD_ADMIN;
import static com.kaiya.api.Constants.ADMIN_DETAILS_REQUIRED;
import static com.kaiya.api.Constants.GET_ADMIN;
import static com.kaiya.api.Constants.ADD_ADMIN;
import static com.kaiya.api.Constants.UPDATE_ADMIN;
import static com.kaiya.api.Constants.VALIDATE_ADMIN_OTP;
import static com.kaiya.api.Constants.DELETE_ADMIN;
import static com.kaiya.api.Constants.GET_ADMIN_BY_USERID;
import static com.kaiya.api.Constants.RESET_PASSWORD_ADMIN;
import static com.kaiya.api.Constants.RESET_PASSWORD_EMAIL_LINK_ADMIN;
import static com.kaiya.api.Constants.GET_ALL_ADMIN;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kaiya.entity.AdminDetails;
import com.kaiya.exceptions.AdminDetailException;
import com.kaiya.model.Changepassword;
import com.kaiya.model.ResetPassword;
import com.kaiya.service.AdminService;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/admin")
@Api(tags = "Admin Details", description = "Admin Api's")
public class AdminDetailsController {

	private static Logger LOGGER = LogManager.getLogger(AdminDetailsController.class);

	@Autowired
	AdminService adminService;

	@RequestMapping(value = ADD_ADMIN, method = RequestMethod.POST)
	private ResponseEntity<?> addAdmin(@RequestBody AdminDetails adminDetails) {

		LOGGER.info("Inside add admin");
		if (adminDetails == null) {
			throw new AdminDetailException(ADMIN_DETAILS_REQUIRED);
		}

		return adminService.addAdmin(adminDetails);

	}

	@RequestMapping(value = UPDATE_ADMIN, method = RequestMethod.PUT)

	private ResponseEntity<?> updateAdmin(@RequestBody AdminDetails updatedAdminDetails) {
		LOGGER.info("Update Admin");
		return adminService.updateAdminDetails(updatedAdminDetails);

	}

	@PostMapping(CHANGE_PASSWORD_ADMIN)
	private ResponseEntity<?> changePassword(@RequestBody Changepassword changePassword) {
		LOGGER.info("Inside Change Password");

		return adminService.changePassword(changePassword);

	}

	@GetMapping(GET_ADMIN + "/{emailId}")
	public ResponseEntity<?> getAdminDetails(@PathVariable("emailId") String emailId) {
		LOGGER.info("Inside validate admin and get admin details");

		return adminService.validateAdmin(emailId);
	}

	@GetMapping(GET_ALL_ADMIN)
	public ResponseEntity<?> getAllAdminDetails() {
		LOGGER.info("Inside get all admin details");

		return adminService.getAllAdminDetails();
	}

	@GetMapping(GET_ADMIN_BY_USERID + "/{userId}")
	public ResponseEntity<?> getAdminDetailsByUserId(@PathVariable("userId") String userId) {

		LOGGER.info("Inside get admin details by userId");

		return adminService.getAdminDetailsByUserId(userId);
	}

	@DeleteMapping(DELETE_ADMIN + "/{userId}")
	public ResponseEntity<?> deleteAdmin(@PathVariable("userId") String userId) {

		LOGGER.info("Inside delete admin details by userId");

		return adminService.deleteByUserId(userId);
	}

	@PostMapping(VALIDATE_ADMIN_OTP)
	private ResponseEntity<?> validateOTP(@RequestBody AdminDetails adminDetails) {
		LOGGER.info("Inside Validate OTP");

		return adminService.validateOTP(adminDetails);

	}

	@PostMapping(RESET_PASSWORD_ADMIN)
	private ResponseEntity<?> resetPassword(@RequestBody ResetPassword resetPassword) {
		LOGGER.info("Inside Reset Password");

		return adminService.resetPassword(resetPassword);

	}

	@PostMapping(RESET_PASSWORD_EMAIL_LINK_ADMIN)
	private ResponseEntity<?> sendResetPasswordOtp(@RequestBody AdminDetails resetPassword) {
		LOGGER.info("Inside Reset Password otp controller");

		return adminService.sendResetPasswordOtp(resetPassword);

	}
}
