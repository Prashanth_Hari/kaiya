package com.kaiya.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(origins = "*")
@Controller
public class PrivacyPolicyController {

	@RequestMapping(value = "/privacy-policy", method = RequestMethod.GET)
	public String privacyPolicy() {
		return "jsp/PrivacyPolicy";
	}

}
