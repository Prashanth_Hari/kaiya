package com.kaiya.controller;

import static com.kaiya.api.Constants.CREATE_INVOICE;
import static com.kaiya.api.Constants.GET_ALL_INVOICE;
import static com.kaiya.api.Constants.GET_INVOICE_BY_CUSTOMER_ID;
import static com.kaiya.api.Constants.GET_INVOICE_BY_INVOICE_ID;
import static com.kaiya.api.Constants.UPDATE_INVOICE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.kaiya.entity.InvoiceDetails;
import com.kaiya.service.InvoiceService;

import io.swagger.annotations.Api;
import java.io.File;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import java.io.IOException;

@RestController
@CrossOrigin(origins = "*")
@Api(tags = "Invoice Details", description = "Invoice Api's")

public class InvoiceDetailsController {

	@Autowired
	InvoiceService invoiceService;

	@GetMapping(path = GET_ALL_INVOICE)
	public ResponseEntity<?> getAllInvoice() {

		return invoiceService.getAllInvoice();
	}

	@GetMapping(path = GET_INVOICE_BY_CUSTOMER_ID + "/{customerId}")
	public ResponseEntity<?> getInvoiceDetailsByCustomerId(@PathVariable("customerId") String customerId) {

		return invoiceService.getInvoiceByCustomerId(customerId);
	}

	@GetMapping(path = GET_INVOICE_BY_INVOICE_ID + "/{invoiceId}")
	public ResponseEntity<?> getInvoiceDetailsByInvoiceid(@PathVariable("invoiceId") String invoiceId) {

		return invoiceService.getInvoiceByInvoiceId(invoiceId);
	}

	@PostMapping(path = CREATE_INVOICE, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> createInvoice(@RequestPart("invoiceDetails") String invoiceDetails,
			@RequestPart("file") MultipartFile file) {

		InvoiceDetails invoiceJson = invoiceService.getJson(invoiceDetails);
		return invoiceService.createInvoice(invoiceJson, file);
	}

	@PutMapping(path = UPDATE_INVOICE, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> updateInvoice(@RequestPart("invoiceDetails") String invoiceDetails,
			@RequestPart("file") MultipartFile file) {
		InvoiceDetails invoiceJson = invoiceService.getJson(invoiceDetails);
		return invoiceService.updateInvoice(invoiceJson, file);
	}

	@GetMapping(path = "/sendMailwithAttachmenet")
	public void sendMailWithAttachement() throws IOException {
		// Creating PDF document object
		PDDocument docs = new PDDocument();
		PDPage blankPage = new PDPage();

		// Adding the blank page to the document
		docs.addPage(blankPage);

		// Saving the document
		docs.save("D:\\kaiya\\test.pdf");

		System.out.println("PDF created");

		// Closing the document
		docs.close();
		// Loading an already existing pdf document
		File file = new File("D:\\kaiya\\test.pdf");
		PDDocument doc = PDDocument.load(file);

		// Retrieve the page
		PDPage page = doc.getPage(0);

		// Creating Object of PDImageXObject for selecting
		// Image and provide the path of file in argument
		PDImageXObject pdImage = PDImageXObject.createFromFile("D:/kaiya/Invoice DetailsINV838383.png", doc);

		// Creating the PDPageContentStream Object
		// for Inserting Image
		PDPageContentStream image = new PDPageContentStream(doc, page);

		// set the Image inside the Page
		image.drawImage(pdImage, 0, 0, 100, 43);
		System.out.println("Image Inserted");

		// Closing the page of PDF by closing
		// PDPageContentStream Object
		// && Saving the Document
		image.close();
		doc.save("D:\\kaiya\\test.pdf");

		// Closing the Document
		doc.close();
		System.out.println("done");

	}

}
