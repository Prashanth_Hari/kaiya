package com.kaiya.controller;

import static com.kaiya.api.Constants.ADD_CART;
import static com.kaiya.api.Constants.UPDATE_CART;
import static com.kaiya.api.Constants.GET_CART;
import static com.kaiya.api.Constants.DELETE_CART;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kaiya.entity.CartDetails;
import com.kaiya.service.CartService;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin(origins = "*")
@Api(tags = "Cart Details", description = "Cart Details Api's")
public class CartController {

	@Autowired
	private CartService cartService;

	private static Logger LOGGER = LogManager.getLogger(CartController.class);

	@GetMapping(GET_CART + "/{customerId}")
	public ResponseEntity<?> getCarts(@PathVariable("customerId") String customerId) {
		LOGGER.info("Inside Get Carts");

		return cartService.getCartDetails(customerId);
	}

	@PostMapping(ADD_CART)
	public ResponseEntity<?> addToCart(@RequestBody CartDetails cartDetails) {

		LOGGER.info("Inside add to cart");

		return cartService.addToCart(cartDetails);

	}

	@PutMapping(UPDATE_CART)
	public ResponseEntity<?> updateCart(@RequestBody CartDetails cartDetails) {
		LOGGER.info("Inside update cart");

		return cartService.updateCart(cartDetails);

	}

	@DeleteMapping(DELETE_CART + "/{cartId}" + "/{customerId}")
	public ResponseEntity<?> deleteCart(@PathVariable("cartId") String cartId,
			@PathVariable("customerId") String customerId) {

		LOGGER.info("Inside deleteCart");

		return cartService.deleteCart(cartId, customerId);
	}

}
