package com.kaiya.controller;

import static com.kaiya.api.Constants.GET_ALL_ADDRESS;
import static com.kaiya.api.Constants.DELETE_ADDRESS;
import static com.kaiya.api.Constants.GET_ADDRESS_BY_ID;
import static com.kaiya.api.Constants.GET_ADDRESS_BY_CUSTOMER_ID;
import static com.kaiya.api.Constants.CREATE_ADDRESS;
import static com.kaiya.api.Constants.UPDATE_ADDRESS;
import static com.kaiya.api.Constants.UPDATE_DEFAULT_ADDRESS;
import static com.kaiya.api.Constants.GET_DEFAULT_ADDRESS;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kaiya.entity.Address;
import com.kaiya.service.AddressService;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin(origins = "*")
@Api(tags = "Address Details", description = "Address Api's")
public class AddressDetailsController {

	private static Logger LOGGER = LogManager.getLogger(AddressDetailsController.class);

	@Autowired
	AddressService addressService;

	@PostMapping(CREATE_ADDRESS)
	private ResponseEntity<?> saveAddress(@RequestBody Address address) {

		LOGGER.info("Inside Create address");

		return addressService.saveAddress(address);

	}

	@PutMapping(UPDATE_ADDRESS)
	private ResponseEntity<?> updateAddress(@RequestBody Address updateAddress) {
		LOGGER.info("Inside Update Address");
		return addressService.updateAddress(updateAddress);

	}

	@PutMapping(UPDATE_DEFAULT_ADDRESS)
	private ResponseEntity<?> updateDefaultAddress(@RequestBody Address updateAddress) {

		LOGGER.info("Inside Update default  address");

		return addressService.updateDefaultAddress(updateAddress);

	}

	@GetMapping(GET_ADDRESS_BY_ID + "/{addressId}")
	public Address getAddressById(@PathVariable("addressId") String addressId) {

		LOGGER.info("Inside Get address by addressId");

		return addressService.getAddressDetails(addressId);
	}

	@GetMapping(GET_ADDRESS_BY_CUSTOMER_ID + "/{customerId}")
	public ResponseEntity<?> getAddressByCustomerId(@PathVariable("customerId") String customerId) {
		LOGGER.info("Inside Get address by customerId");

		return addressService.getAddressDetailsByCustomerId(customerId);
	}

	@GetMapping(GET_DEFAULT_ADDRESS + "/{customerId}")
	public ResponseEntity<?> getDefaultAddress(@PathVariable("customerId") String customerId) {

		LOGGER.info("Inside Get default Address");

		return addressService.getDefaultAddress(customerId);
	}

	@GetMapping(GET_ALL_ADDRESS)
	public Iterable getAllAddress() {

		LOGGER.info("Inside Get All Address");

		return addressService.getAllAddress();
	}

	@DeleteMapping(DELETE_ADDRESS + "/{addressId}")
	public ResponseEntity<?> deleteAddress(@PathVariable("addressId") String addressId) {

		LOGGER.info("Inside Delete Address");

		return addressService.deleteAddress(addressId);
	}
}
