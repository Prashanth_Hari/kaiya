package com.kaiya.controller;

import static com.kaiya.api.Constants.ADD_PRODUCT;
import static com.kaiya.api.Constants.CREATE_INVOICE;
import static com.kaiya.api.Constants.GET_PRODUCTS;
import static com.kaiya.api.Constants.GET_ALL_PRODUCTS;

import static com.kaiya.api.Constants.UPDATE_PRODUCT;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.kaiya.entity.ProductDetails;
import com.kaiya.service.ProductService;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin(origins = "*")
@Api(tags = "Product Details", description = "Product Details Api's")

public class ProductDetailsController {

	private static Logger LOGGER = LogManager.getLogger(ProductDetailsController.class);

	@Autowired
	ProductService productService;

	@GetMapping(GET_ALL_PRODUCTS)

	public List<ProductDetails> getAllProducts() {

		LOGGER.info("Inside get all products");

		return productService.getAllProductDetails();
	}

	@GetMapping(GET_PRODUCTS + "/{customerId}")

	public List<ProductDetails> getProducts(@PathVariable("customerId") String customerId) {

		LOGGER.info("Inside get products by customerId");

		return productService.getProductDetails(customerId);
	}

	@PostMapping(path = ADD_PRODUCT, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> addProduct(@RequestPart("productDetails") String productDetails,
			@RequestPart("file") MultipartFile file) {

		LOGGER.info("Inside add products");
		ProductDetails newProductDetail = productService.getJson(productDetails);
		return productService.addProduct(newProductDetail, file);

	}

	@PutMapping(path = UPDATE_PRODUCT, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> updateProduct(@RequestPart("productDetails") String productDetails,
			@RequestPart("file") MultipartFile file) {

		LOGGER.info("Inside updateProduct");
		ProductDetails updateProductDetail = productService.getJson(productDetails);
		return productService.updateProduct(updateProductDetail, file);

	}

	@DeleteMapping(path = "/deleteProduct/{productId}")
	public ResponseEntity<?> deleteProduct(@PathVariable("productId") String productId) {

		LOGGER.info("Inside deleteProduct");

		return productService.deleteByProductId(productId);
	}

}
