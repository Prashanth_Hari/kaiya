package com.kaiya.controller;

import static com.kaiya.api.Constants.GET_CUSTOMER_CARE_DETAILS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;

import com.kaiya.service.CustomerCareService;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin(origins = "*")
@Api(tags = "CustomerCare Details", description = "Customer Api's")
public class CustomerCareController {

	@Autowired
	private CustomerCareService customerCareService;

	private static Logger LOGGER = LogManager.getLogger(CustomerCareController.class);

	@GetMapping(GET_CUSTOMER_CARE_DETAILS)
	public ResponseEntity<?> getCustomerCareDetails() {
		LOGGER.info("Inside get customer care details");

		return customerCareService.getCustomerCareDetails();
	}

}
