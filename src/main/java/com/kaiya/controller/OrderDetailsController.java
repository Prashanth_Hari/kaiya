package com.kaiya.controller;

import static com.kaiya.api.Constants.GET_ORDERS;
import static com.kaiya.api.Constants.GET_ORDERS_BY_ORDERID;
import static com.kaiya.api.Constants.GET_RECENT_ORDERS;
import static com.kaiya.api.Constants.CREATE_ORDER;
import static com.kaiya.api.Constants.DELETE_ORDER;
import static com.kaiya.api.Constants.DELETE_SUB_ORDER;
import static com.kaiya.api.Constants.UPDATE_ORDER;
import static com.kaiya.api.Constants.CANCEL_ORDER;

import static com.kaiya.api.Constants.GET_ORDERS_BY_PHONE_NUMBER;
import static com.kaiya.api.Constants.GET_ORDERS_BY_EMAILID;
import static com.kaiya.api.Constants.GET_ORDERS_BY_DATE;
import static com.kaiya.api.Constants.GET_ORDERS_BY_DATE_RANGE;
import static com.kaiya.api.Constants.GET_TOTAL_ORDERS_COUNT;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kaiya.entity.Orders;
import com.kaiya.service.OrderService;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin(origins = "*")
@Api(tags = "Order Details", description = "OrderDetails Api's")

public class OrderDetailsController {

	@Autowired
	OrderService orderService;

	private static Logger LOGGER = LogManager.getLogger(OrderDetailsController.class);

	@GetMapping(path = GET_ORDERS)
	public ResponseEntity<?> getOrders() {
		LOGGER.info("Inside get all orders");

		return orderService.getOrders();
	}

	@GetMapping(path = GET_ORDERS_BY_ORDERID + "/{orderId}")
	public ResponseEntity<?> getOrdersByOrderId(@PathVariable("orderId") String orderId) {

		LOGGER.info("Inside get orders by orderId");

		return orderService.getOrdersByOrderId(orderId);
	}

	@GetMapping(path = GET_ORDERS_BY_PHONE_NUMBER + "/{phoneNumber}")
	public ResponseEntity<?> getOrdersByPhoneNumber(@PathVariable("phoneNumber") String phoneNumber) {

		LOGGER.info("Inside get orders by phoneNumber");

		return orderService.getOrdersByPhoneNumber(phoneNumber);
	}

	@GetMapping(path = GET_ORDERS_BY_EMAILID + "/{emailId}")
	public ResponseEntity<?> getOrdersByEmailId(@PathVariable("emailId") String emailId) {
		LOGGER.info("Inside getOrdersByEmailId");

		return orderService.getOrdersByEmailId(emailId);
	}

	@GetMapping(path = GET_ORDERS_BY_DATE + "/{date}")
	public ResponseEntity<?> getOrdersByDate(@PathVariable("date") String date) {
		LOGGER.info("Inside getOrdersByDate");

		return orderService.getOrdersByDate(date);
	}

	@GetMapping(path = GET_ORDERS_BY_DATE_RANGE + "/{fromDate}/{toDate}")
	public ResponseEntity<?> getOrdersByDateRange(@PathVariable("fromDate") String fromDate,
			@PathVariable("toDate") String toDate) {
		LOGGER.info("Inside getOrdersByDateRange");

		return orderService.getOrdersByDateRange(fromDate, toDate);
	}

	@GetMapping(path = GET_ORDERS + "/{customerId}")
	public ResponseEntity<?> getOrder(@PathVariable("customerId") String customerId) {

		LOGGER.info("Inside getOrder by customerId");

		return orderService.getOrder(customerId);
	}

	@GetMapping(path = GET_RECENT_ORDERS + "/{customerId}")
	public ResponseEntity<?> getRecentOrderById(@PathVariable("customerId") String customerId) {
		LOGGER.info("Inside getRecentOrder by customerId");

		return orderService.getRecentOrderById(customerId);
	}

	@GetMapping(path = GET_RECENT_ORDERS)
	public ResponseEntity<?> getRecentOrders() {

		LOGGER.info("Inside getRecentOrders");

		return orderService.getRecentOrders();
	}

	@GetMapping(path = GET_TOTAL_ORDERS_COUNT)
	public ResponseEntity<?> getTotalOrdersCount() {

		LOGGER.info("Inside getTotalOrdersCount");

		return orderService.getTotalOrdersCount();
	}

	@PostMapping(path = CREATE_ORDER)
	public ResponseEntity<?> createeOrder(@RequestBody Orders newOrders) {

		LOGGER.info("Inside create order");
		return orderService.createOrders(newOrders);
	}

	@PutMapping(path = UPDATE_ORDER)

	public ResponseEntity<?> updateOrder(@RequestBody Orders updateOrder) {

		LOGGER.info("Inside update order");

		return orderService.updateOrder(updateOrder);
	}

	@PutMapping(path = CANCEL_ORDER)
	public ResponseEntity<?> cancelOrder(@RequestBody Orders updateOrder) {
		LOGGER.info("Inside cancel order");

		return orderService.updateOrder(updateOrder);
	}

	@DeleteMapping(path = DELETE_ORDER + "/{id}")
	public ResponseEntity<?> deleteOrder(@PathVariable("id") String id) {
		LOGGER.info("Inside delete order");

		return orderService.deleteByOrderId(id);
	}

	@DeleteMapping(path = DELETE_SUB_ORDER + "/{id}")
	public ResponseEntity<?> deleteSubOrder(@PathVariable("id") String id) {
		return orderService.deleteBySubOrderId(id);
	}

}
