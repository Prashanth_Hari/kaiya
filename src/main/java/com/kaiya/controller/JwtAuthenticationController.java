package com.kaiya.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kaiya.api.JwtTokenUtil;
import com.kaiya.exceptions.CustomerDetailException;
import com.kaiya.exceptions.InvalidCredentialException;
import com.kaiya.model.JwtRequest;
import com.kaiya.model.JwtResponse;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin(origins = "*")
@Api(tags = "Authentication", description = "Authentication Api's")
@RequestMapping("/authenticate")
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService userDetailsService;

	@RequestMapping(value = "/customer", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationTokenForCustomer(@RequestBody JwtRequest authenticationRequest)
			throws Exception {

		if (authenticationRequest.getUsername() == null || authenticationRequest.getUsername().equals("")) {
			throw new CustomerDetailException("Username is required");
		} else if (authenticationRequest.getPassword() == null || authenticationRequest.getPassword().equals("")) {
			throw new CustomerDetailException("Password is required");
		}
		authenticate(authenticationRequest.getUsername() + ":Customer", authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername() + ":Customer");

		final String token = jwtTokenUtil.generateToken(userDetails, "Customer");

		return ResponseEntity.ok(new JwtResponse(token));
	}

	@RequestMapping(value = "/admin", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationTokenForAdmin(@RequestBody JwtRequest authenticationRequest)
			throws Exception {

		if (authenticationRequest.getUsername() == null || authenticationRequest.getUsername().equals("")) {
			throw new CustomerDetailException("Username is required");
		} else if (authenticationRequest.getPassword() == null || authenticationRequest.getPassword().equals("")) {
			throw new CustomerDetailException("Password is required");
		}

		authenticate(authenticationRequest.getUsername() + ":Admin", authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername() + ":Admin");

		final String token = jwtTokenUtil.generateToken(userDetails, "Admin");

		return ResponseEntity.ok(new JwtResponse(token));
	}

	private void authenticate(String username, String password) throws Exception {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new InvalidCredentialException("Invalid Credential");
		}
	}

}
