package com.kaiya.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kaiya.entity.OrderDetails;

@Repository
public interface OrderDetailsRepository extends CrudRepository<OrderDetails, String> {

}
