package com.kaiya.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kaiya.entity.CustomerDetails;

@Repository
public interface CustomerDetailsRepository extends CrudRepository<CustomerDetails, String> {

	@Query("select s from customer_details s where s.emailId = ?1")
	public CustomerDetails getCustomerDetail(String emailId);

	@Query("select s from customer_details s where s.emailId = ?1")
	public Optional<CustomerDetails> findByEmailId(String emailId);

	@Query("select s from customer_details s where s.mobileNumber = ?1")
	public Optional<CustomerDetails> findByPhoneNumber(String mobileNumber);

	@Query("select s from customer_details s where s.gst_Id = ?1")
	public CustomerDetails findByGstId(String gstId);

	public List<CustomerDetails> findAll();
}
