package com.kaiya.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kaiya.entity.OrderStatstics;

@Repository
public interface OrderStasticsRepository extends CrudRepository<OrderStatstics, String> {

	@Query(nativeQuery = true, value = "select * from order_statstics")
	public List<OrderStatstics> findAllOrderStastics();
}
