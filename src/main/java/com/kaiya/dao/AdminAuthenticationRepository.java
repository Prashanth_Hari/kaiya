package com.kaiya.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kaiya.entity.AdminDetails;

@Repository
public interface AdminAuthenticationRepository extends CrudRepository<AdminDetails, String> {

	@Query("select s from admin_details s where s.emailId = ?1")
	public AdminDetails findByUsername(String userName);

	@Query("select s from admin_details s where s.mobileNumber = ?1")
	public Optional<AdminDetails> findByPhoneNumber(String mobileNumber);

	public List<AdminDetails> findAll();
}
