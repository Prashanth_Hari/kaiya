package com.kaiya.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kaiya.entity.Orders;
import com.kaiya.model.OrderStatsticsCount;

@Repository
public interface OrdersRepository extends CrudRepository<Orders, String> {

	@Query(nativeQuery = true, value = "select * from orders order by created_date DESC")
	public List<Orders> findAllOrders();

	@Query("select o from orders o where o.id = :orderId")
	public List<Orders> findOrdersByOrderId(String orderId);

	@Query("select o from orders o where o.customerDetails.customerId = :customerId order by created_date DESC")
	public List<Orders> findOrdersById(String customerId);

	@Query("select o from orders o where DATE(created_date) = DATE(:date) order by created_date DESC")
	public List<Orders> findOrdersByDate(String date);

	@Query("select o from orders o where (DATE(created_date) BETWEEN DATE(:fromDate) AND  DATE(:toDate)) order by created_date DESC")
	public List<Orders> findOrdersByDateRange(String fromDate, String toDate);

	@Query(nativeQuery = true, value = "select * from orders where customer_id = ?1 AND created_date >= last_day(now()) + interval 1 day - interval 2 month order by created_date desc")
	public List<Orders> findRecentOrdersById(String customerId);

	@Query(nativeQuery = true, value = "select * from orders where created_date>= last_day(now()) + interval 1 day - interval 2 month order by created_date desc")
	public List<Orders> findRecentOrders();

	// @Query(nativeQuery = true, value = "select COUNT(*) from orders where
	// comments =?1")
	@Query(nativeQuery = true, value = "select sum(case when comments = 'CREATED' then 1 else 0 end) as created,sum(case when comments = 'Cancelled' then 1 else 0 end) as cancelled,sum(case when comments = 'Accepted' then 1 else 0 end) as accepted,sum(case when comments = 'Delivered' then 1 else 0 end) as delivered from orders")
	public Object findTotalOrdersCount(String comments);
}
