package com.kaiya.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kaiya.entity.JWTUserAuthentication;

@Repository
public interface UserAuthenticationRepository extends CrudRepository<JWTUserAuthentication, Long> {

	@Query("select  s from  JWTUserAuthentication s where s.username = ?1")
	public JWTUserAuthentication findByUsername(String Username);
}
