package com.kaiya.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kaiya.entity.InvoiceDetails;

@Repository
public interface InvoiceRepository extends CrudRepository<InvoiceDetails, String> {

	@Query("select o from invoice_details o where o.customerId = ?1")
	public List<InvoiceDetails> findInvoiceByCustomerId(String customerId);
}
