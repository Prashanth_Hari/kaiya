package com.kaiya.dao;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kaiya.entity.ProductDetails;

@Repository
public interface ProductDetailsRepository extends CrudRepository<ProductDetails, String> {

	@Query("select s from product_details s where s.productName = ?1")
	public ProductDetails findByProductName(String productName);

	List<ProductDetails> findAll();

	default Map<String, ProductDetails> findAllMap() {
		return findAll().stream().collect(Collectors.toMap(ProductDetails::getProductId, v -> v));
	}
}
