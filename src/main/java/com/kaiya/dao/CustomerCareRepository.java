package com.kaiya.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kaiya.entity.CustomerCareDetails;
import com.kaiya.entity.ProductDetails;

@Repository
public interface CustomerCareRepository extends CrudRepository<CustomerCareDetails, Long> {

	List<CustomerCareDetails> findAll();
}
