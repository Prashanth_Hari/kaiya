package com.kaiya.dao;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kaiya.entity.CartDetails;

@Repository
public interface CartDetailsRepository extends CrudRepository<CartDetails, String> {

	@Query("select s from cart_details s where s.customerId = ?1")
	public List<CartDetails> findByCustomerId(String customerId);

	@Query("select SUM(totalAmount) AS totalCartAmount from cart_details s where s.customerId = :customerId AND s.productDetails.status = :productStatus")
	public Integer findCartAmount(String customerId, String productStatus);

	@Transactional
	@Modifying
	@Query("delete from cart_details s where s.customerId=?1")
	public void deleteCartByCustomerId(String customerId);

	@Transactional
	@Modifying
	@Query("delete from cart_details s where s.productDetails.productId=?1")
	public void deleteCartByProductId(String productId);

	@Query("select s from cart_details s where s.productDetails.productId = ?1")
	public Optional<CartDetails> findByProductId(String productId);
}
