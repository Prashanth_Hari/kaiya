package com.kaiya.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kaiya.entity.Address;

@Repository
public interface AddressDetailsRepository extends CrudRepository<Address, String> {

	@Query("select o from address_details o where o.customerDetails.customerId = :customerId order by addressDefaultValue DESC")
	public List<Address> findAddressByCustomerId(String customerId);
	
	@Query("select o from address_details o where o.customerDetails.customerId = :customerId AND o.addressDefaultValue=:defaultValue")
	public Optional<Address> findDefaultAddress(String customerId,String defaultValue);

}
