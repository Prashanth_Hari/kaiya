package com.kaiya.model;

import java.math.BigDecimal;

public class OrderStatsticsCount {

	public BigDecimal getCreated() {
		return created;
	}

	public void setCreated(BigDecimal created) {
		this.created = created;
	}

	public BigDecimal getCancelled() {
		return cancelled;
	}

	public void setCancelled(BigDecimal cancelled) {
		this.cancelled = cancelled;
	}

	private BigDecimal created;

	private BigDecimal cancelled;

}
