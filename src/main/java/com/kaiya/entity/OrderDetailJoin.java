package com.kaiya.entity;

public class OrderDetailJoin {

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getOrderDetailID() {
		return orderDetailID;
	}

	public void setOrderDetailID(String orderDetailID) {
		this.orderDetailID = orderDetailID;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	private String quantity;

	private String status;

	private String createdDate;

	private String updatedDate;

	private String createdBy;

	private String updatedBy;

	private String customerID;

	private String orderID;

	private String orderDetailID;

	private String productId;

	private String productName;

	public OrderDetailJoin(String orderID, String orderDetailID, String customerID, String createdDate,
			String updatedDate, String createdBy, String updatedBy, String status, String quantity, String productId,
			String productName) {
		this.productName = productName;
		this.productId = productId;
		this.orderDetailID = orderDetailID;
		this.orderID = orderID;
		this.customerID = customerID;
		this.quantity = quantity;

		this.status = status;

		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;

	}

	public OrderDetailJoin() {

	}
}
