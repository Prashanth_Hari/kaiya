package com.kaiya.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity(name = "product_details")
public class ProductDetails implements Serializable {

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	private static final long serialVersionUID = 1L;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Id
	@Column(name = "product_id")
	private String productId;

	@Column(name = "product_name")
	private String productName;

	@Column(name = "product_img")
	private String productImage;

	private String litres;

	@Transient
	private boolean isCartClicked;
	@Transient
	private int quantity;

	@Transient
	private String cartId;

	private Double amount;

	private String status;

	@Column(name = "offer_amount")
	private Double offerAmount;

	@Column(name = "img_extension")
	private String imgExtension;

	@Transient
	@JsonProperty
	private boolean isImageChanged;

	public boolean isImageChanged() {
		return isImageChanged;
	}

	public void setImageChanged(boolean isImageChanged) {
		this.isImageChanged = isImageChanged;
	}

	public String getProductImageKey() {
		return productImageKey;
	}

	public void setProductImageKey(String productImageKey) {
		this.productImageKey = productImageKey;
	}

	private int min;

	private int max;

	@Column(name = "product_image_key")
	private String productImageKey;

	public String getImgExtension() {
		return imgExtension;
	}

	public void setImgExtension(String imgExtension) {
		this.imgExtension = imgExtension;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public String getLitres() {
		return litres;
	}

	public void setLitres(String litres) {
		this.litres = litres;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public boolean getIsCartClicked() {
		return isCartClicked;
	}

	public void setIsCartClicked(boolean isCartClicked) {
		this.isCartClicked = isCartClicked;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getOfferAmount() {
		return offerAmount;
	}

	public void setOfferAmount(Double offerAmount) {
		this.offerAmount = offerAmount;
	}

	public class Response {

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		private int code;

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		private String status;

		private String message;

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

	}
}
