package com.kaiya.entity;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity(name = "invoice_details")
public class InvoiceDetails {

	@Id
	@Column(name = "invoice_id")
	private String invoiceId;

	@Column(name = "invoice_img")
	private String invoiceImage;

	@Transient
	private String orderId;

	@Transient
	@JsonProperty
	private boolean isImageChanged;

	@Column(name = "invoice_image_key")
	private String invoiceImageKey;

	public String getInvoiceImageKey() {
		return invoiceImageKey;
	}

	public void setInvoiceImageKey(String invoiceImageKey) {
		this.invoiceImageKey = invoiceImageKey;
	}

	public boolean isImageChanged() {
		return isImageChanged;
	}

	public void setImageChanged(boolean isImageChanged) {
		this.isImageChanged = isImageChanged;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "created_date")
	private String createdDate;

	private String comments;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "customer_id")
	private String customerId;

	@Column(name = "updated_date")
	private String updatedDate;

	@Column(name = "updated_by")
	private String updatedBy;

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getInvoiceImage() {
		return invoiceImage;
	}

	public void setInvoiceImage(String invoiceImage) {
		this.invoiceImage = invoiceImage;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public class Response {

		private int code;

		private String status;

		private String message;

		private InvoiceDetails data;

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public InvoiceDetails getData() {
			return data;
		}

		public void setData(InvoiceDetails data) {
			this.data = data;
		}

	}
}
