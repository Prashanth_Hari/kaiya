package com.kaiya.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "cart_details")
public class CartDetails {

	@Id
	@Column(name = "cart_id")
	private String cartId;

	@OneToOne
	@JoinColumn(name = "product_id")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private ProductDetails productDetails;

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getOfferAmount() {
		return offerAmount;
	}

	public void setOfferAmount(Double offerAmount) {
		this.offerAmount = offerAmount;
	}

	private int quantity;

	@Column(name = "created_date")
	private String createdDate;

	@Column(name = "customer_id")
	private String customerId;

	private String litres;

	private Double amount;

	@Column(name = "offer_amount")
	private Double offerAmount;

	@Column(name = "total_amount")
	private Double totalAmount;

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getLitres() {
		return litres;
	}

	public void setLitres(String litres) {
		this.litres = litres;
	}

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public ProductDetails getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(ProductDetails productDetails) {
		this.productDetails = productDetails;
	}

	public class Response {

		private int code;

		private String status;

		private String message;

		private int totalNumberOfItem;

		private List<CartDetails> cartDetails;

		private int totalCartAmount;

		public int getTotalCartAmount() {
			return totalCartAmount;
		}

		public void setTotalCartAmount(int totalCartAmount) {
			this.totalCartAmount = totalCartAmount;
		}

		public int getTotalNumberOfItem() {
			return totalNumberOfItem;
		}

		public List<CartDetails> getCartDetails() {
			return cartDetails;
		}

		public void setCartDetails(List<CartDetails> cartDetails) {
			this.cartDetails = cartDetails;
		}

		public void setTotalNumberOfItem(int totalNumberOfItem) {
			this.totalNumberOfItem = totalNumberOfItem;
		}

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

	}

}
