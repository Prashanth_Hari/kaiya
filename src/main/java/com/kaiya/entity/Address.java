package com.kaiya.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity(name = "address_details")
public class Address {

	@Id
	private String id;

	private String address;

	@Column(name = "email")
	private String emailId;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@OneToOne(optional = false)
	@JoinColumn(name = "customer_id")
	@JsonBackReference
	private CustomerDetails customerDetails;

	private String name;

	@Column(name = "phone_number")
	private String phoneNumber;

	private String state;

	private String pincode;

	private String city;

	@Column(name = "address_type")
	private String addressType;

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "created_date")
	private String createdDate;

	@Column(name = "is_default_address")
	private String addressDefaultValue;

	public String getAddressDefaultValue() {
		return addressDefaultValue;
	}

	public void setAddressDefaultValue(String addressDefaultValue) {
		this.addressDefaultValue = addressDefaultValue;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public class Response {

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		private int code;

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public Address getDefaultAddress() {
			return defaultAddress;
		}

		public void setDefaultAddress(Address defaultAddress) {
			this.defaultAddress = defaultAddress;
		}

		private String status;

		private Address defaultAddress;

		public List<Address> getAddressDetails() {
			return addressDetails;
		}

		public void setAddressDetails(List<Address> addressDetails) {
			this.addressDetails = addressDetails;
		}

		private String message;

		private List<Address> addressDetails;

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

	}

}
