package com.kaiya.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity(name = "customer_details")
public class CustomerDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "customer_id")
	private String customerId;

	@Column(name = "email")
	private String emailId;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "company_name")
	private String companyName;

	private String password;

	@Column(name = "otp")
	private String otp;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String string) {
		this.otp = string;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getGst_Id() {
		return gst_Id;
	}

	public void setGst_Id(String gst_Id) {
		this.gst_Id = gst_Id;
	}

	@Column(name = "mobile_Number")
	private String mobileNumber;

	@Column(name = "created_date")
	private String createdDate;

	@Column(name = "updated_date")
	private String updatedDate;

	@Column(name = "customer_img")
	private String customerImage;

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

	public String getCustomerImage() {
		return customerImage;
	}

	public void setCustomerImage(String customerImage) {
		this.customerImage = customerImage;
	}

	@Column(name = "last_login")
	private String lastLogin;

	@OneToMany(mappedBy = "customerDetails", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonManagedReference
	private List<Address> address;

	@Column(name = "gst_id")
	private String gst_Id;

	private String role;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public class Response {

		private String status;

		private String message;

		private int code;

		private CustomerDetails customerDetails;

		private String jwttoken;

		public String getJwttoken() {
			return jwttoken;
		}

		public void setJwttoken(String jwttoken) {
			this.jwttoken = jwttoken;
		}

		public CustomerDetails getCustomerDetails() {
			return customerDetails;
		}

		public void setCustomerDetails(CustomerDetails customerDetails) {
			this.customerDetails = customerDetails;
		}

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

	}

}
