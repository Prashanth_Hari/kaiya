package com.kaiya.service;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.kaiya.api.JwtTokenUtil;
import com.kaiya.dao.AdminAuthenticationRepository;
import com.kaiya.entity.AdminDetails;
import com.kaiya.exceptions.AdminDetailException;
import com.kaiya.exceptions.CustomerDetailException;
import com.kaiya.model.Changepassword;
import com.kaiya.model.ResetPassword;
import com.kaiya.util.CommonUtils;
import static com.kaiya.api.Constants.EMAIL_ID_REQUIRED;
import static com.kaiya.api.Constants.EMAIL_SENT_SUCCESSFULLY;
import static com.kaiya.api.Constants.PASSWORD_REQUIRED;
import static com.kaiya.api.Constants.ADMIN_DELETED_MSG;
import static com.kaiya.api.Constants.NEW_PASSWORD_REQUIRED;
import static com.kaiya.api.Constants.CONFIRM_PASSWORD_REQUIRED;
import static com.kaiya.api.Constants.PASSWORD_CHANGED;
import static com.kaiya.api.Constants.ADMIN_CREATED_MSG;
import static com.kaiya.api.Constants.SUCCESS;
import static com.kaiya.api.Constants.EMAIL_ALREADY_EXIST;
import static com.kaiya.api.Constants.EMAIL_DOSENT_EXIST;
import static com.kaiya.api.Constants.ADMIN_NOT_FOUND;
import static com.kaiya.api.Constants.NO_RECORDS_CHANGED;
import static com.kaiya.api.Constants.OTP_INVALID_MSG;
import static com.kaiya.api.Constants.OTP_VALID_MSG;
import static com.kaiya.api.Constants.USER_ID_REQUIRED;
import static com.kaiya.api.Constants.ADMIN_UPDATED_MSG;
import static com.kaiya.api.Constants.ADMIN_ROLE;
import static com.kaiya.api.Constants.MOBILENUMBER_ALREADY_EXIST;
import static com.kaiya.api.Constants.NO_RECORDS_FOUND;

@Service
public class AdminService {

	private static Logger LOGGER = LogManager.getLogger(AdminService.class);

	@Autowired
	private AdminAuthenticationRepository adminAuthenticationRepository;

	@Autowired
	private CommonUtils commonUtils;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private EmailServiceImpl emailServiceImpl;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService userDetailsService;

	/**
	 * This method is used to store the admin details to the database.
	 * 
	 * @param adminDetails
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> addAdmin(AdminDetails adminDetails) {
		LOGGER.info("Inside of addAdmin");
		ResponseEntity<Object> responseEntity = null;
		AdminDetails createdAdminDetails = null;
		try {
			if (adminDetails.getUserName() == null || adminDetails.getUserName().equals("")) {
				throw new AdminDetailException(EMAIL_ID_REQUIRED);
			} else if (adminDetails.getPassword() == null || adminDetails.getPassword().equals("")) {
				throw new AdminDetailException(PASSWORD_REQUIRED);
			}
			if (fetchByUsername(adminDetails.getEmailId()) != null)
				throw new AdminDetailException(EMAIL_ALREADY_EXIST);

			if (fetchUserByMobileNumber(adminDetails.getMobileNumber()) != null)
				throw new AdminDetailException(MOBILENUMBER_ALREADY_EXIST);

			String encryptedPwd = bCryptPasswordEncoder.encode(adminDetails.getPassword());
			adminDetails.setUserId("ADMN" + commonUtils.getRandomNumberString());

			adminDetails.setCreatedDate(commonUtils.getDate());
			adminDetails.setUpdatedDate(commonUtils.getDate());
			adminDetails.setRole(ADMIN_ROLE);
			adminDetails.setPassword(encryptedPwd);
			createdAdminDetails = adminAuthenticationRepository.save(adminDetails);

		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Inside admin service" + e);
		}
		if (createdAdminDetails.getUserId() != null) {
			AdminDetails.Response response = new AdminDetails().new Response();
			response.setCode(200);
			response.setMessage(ADMIN_CREATED_MSG);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End of addAdmin");
		return responseEntity;

	}

	/**
	 * This method is used to get the admin details from the databse based on the
	 * provided emailId.
	 * 
	 * @param emailId
	 * @return AdminDetails
	 */
	public AdminDetails fetchByUsername(String emailId) {

		return adminAuthenticationRepository.findByUsername(emailId);

	}

	/**
	 * This method is used to get the admin details based on the mobile number.
	 * 
	 * @param mobileNumber
	 * @return AdminDetails
	 */
	public AdminDetails fetchUserByMobileNumber(String mobileNumber) {
		LOGGER.info("Inside of fetchUserByMobileNumber");
		Optional<AdminDetails> adminDetails = adminAuthenticationRepository.findByPhoneNumber(mobileNumber);
		if (adminDetails.isPresent()) {
			LOGGER.info("End of fetchUserByMobileNumber");
			return adminDetails.get();
		} else {
			LOGGER.info("End of fetchUserByMobileNumber");
			return null;
		}
	}

	/**
	 * This method is used to change the password of admin details and store it to
	 * the database.
	 * 
	 * @param changePassword
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> changePassword(Changepassword changePassword) {
		LOGGER.info("Inside of changePassword");
		ResponseEntity<Object> responseEntity = null;
		AdminDetails adminDetails = null;
		if (changePassword.getUserId() == null || changePassword.getUserId().equals("")) {
			throw new AdminDetailException(USER_ID_REQUIRED);
		} else if (changePassword.getPassword() == null || changePassword.getPassword().equals("")) {
			throw new AdminDetailException(PASSWORD_REQUIRED);
		} else if (changePassword.getNewPassword() == null || changePassword.getNewPassword().equals("")) {
			throw new AdminDetailException(NEW_PASSWORD_REQUIRED);
		} else if (changePassword.getConfirmPassword() == null || changePassword.getConfirmPassword().equals("")) {
			throw new AdminDetailException(CONFIRM_PASSWORD_REQUIRED);
		}
		AdminDetails existingAdmin = getAdminDetails(changePassword.getUserId());
		if (existingAdmin == null) {
			throw new AdminDetailException(ADMIN_NOT_FOUND);
		}
		try {
			if (!bCryptPasswordEncoder.matches(changePassword.getPassword(), existingAdmin.getPassword())) {
				throw new AdminDetailException("Current password is invalid.");
			} else if (bCryptPasswordEncoder.matches(changePassword.getNewPassword(), existingAdmin.getPassword())) {
				throw new AdminDetailException("Current password and New password should not be same.");

			} else {

				existingAdmin.setPassword(bCryptPasswordEncoder.encode(changePassword.getNewPassword()));
				adminDetails = adminAuthenticationRepository.save(existingAdmin);

			}

		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Inside Change password" + e);
		}
		if (getAdminDetails(adminDetails.getUserId()) != null) {
			AdminDetails.Response response = new AdminDetails().new Response();
			response.setCode(200);
			response.setMessage(PASSWORD_CHANGED);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End of changePassword");
		return responseEntity;

	}

	/**
	 * This method is used to validate the otp.
	 * 
	 * @param cutomerDetails
	 * @return
	 */
	public ResponseEntity<?> validateOTP(AdminDetails adminDetails) {
		LOGGER.info("Inside of validateOTP");
		ResponseEntity<Object> responseEntity = null;
		try {
			if (adminDetails.getOtp() == null || adminDetails.getOtp().equals("")) {
				throw new AdminDetailException("OTP is required");
			}
			AdminDetails existingAdmin = fetchUserByEmailId(adminDetails.getEmailId());
			if (existingAdmin == null) {
				throw new AdminDetailException(EMAIL_DOSENT_EXIST);
			}

			if (adminDetails.getOtp().equals(existingAdmin.getOtp())) {
				existingAdmin.setOtp(null);
				existingAdmin = adminAuthenticationRepository.save(existingAdmin);
				if (existingAdmin != null) {
					AdminDetails.Response response = new AdminDetails().new Response();
					response.setCode(200);
					response.setMessage(OTP_VALID_MSG);
					response.setStatus(SUCCESS);
					responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
				}

			} else {
				throw new AdminDetailException(OTP_INVALID_MSG);
			}

		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Exception Occured in validating otp" + e);
		}
		LOGGER.info("End of validateOTP");
		return responseEntity;

	}

	/**
	 * This method is used to send the otp to the registered emailid.
	 * 
	 * @param resetPassword
	 * @return
	 */
	public ResponseEntity<?> sendResetPasswordOtp(AdminDetails resetPassword) {
		LOGGER.info("Inside sendResetPasswordOtp otp");
		ResponseEntity<Object> responseEntity = null;
		try {
			if (resetPassword.getEmailId() == null || resetPassword.getEmailId().equals("")) {
				throw new CustomerDetailException("EmailId is required");
			}
			AdminDetails existingAdmin = fetchUserByEmailId(resetPassword.getEmailId());
			if (existingAdmin == null) {
				throw new AdminDetailException(EMAIL_DOSENT_EXIST);
			}
			existingAdmin.setOtp(commonUtils.generateOTP());
			existingAdmin = adminAuthenticationRepository.save(existingAdmin);
			emailServiceImpl.sendForgotPasswordLink(null, existingAdmin);

			if (existingAdmin != null) {
				AdminDetails.Response response = new AdminDetails().new Response();
				response.setCode(200);
				response.setMessage(EMAIL_SENT_SUCCESSFULLY);
				response.setStatus(SUCCESS);
				responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
			}

		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Exception Occured in reset password otp method" + e);

		}
		LOGGER.info("End of sendResetPasswordOtp");
		return responseEntity;

	}

	/**
	 * This method is used to reset the password and updates the new password to
	 * database.
	 * 
	 * @param changePassword
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> resetPassword(ResetPassword resetPassword) {
		LOGGER.info("Inside of resetPassword");
		ResponseEntity<Object> responseEntity = null;
		if (resetPassword.getEmailId() == null || resetPassword.getEmailId().equals("")) {
			throw new AdminDetailException(EMAIL_ID_REQUIRED);
		} else if (resetPassword.getNewPassword() == null || resetPassword.getNewPassword().equals("")) {
			throw new AdminDetailException(NEW_PASSWORD_REQUIRED);
		}
		AdminDetails existingAdmin = fetchUserByEmailId(resetPassword.getEmailId());
		if (existingAdmin == null) {
			throw new AdminDetailException(ADMIN_NOT_FOUND);
		}
		try {
			existingAdmin.setPassword(bCryptPasswordEncoder.encode(resetPassword.getNewPassword()));
			existingAdmin = adminAuthenticationRepository.save(existingAdmin);

		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Inside Change password" + e);
		}
		if (getAdminDetails(existingAdmin.getUserId()) != null) {
			AdminDetails.Response response = new AdminDetails().new Response();
			response.setCode(200);
			response.setMessage(PASSWORD_CHANGED);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End of resetPassword");
		return responseEntity;

	}

	public ResponseEntity<?> updateAdminDetails(AdminDetails updatedAdminDetails) {
		LOGGER.info("Inside of updateAdminDetails");
		ResponseEntity<Object> responseEntity = null;
		AdminDetails adminDetails = null;
		boolean isEmailAddressUpdated = false;
		if (updatedAdminDetails.getUserId() == null)
			throw new AdminDetailException(USER_ID_REQUIRED);
		AdminDetails existingAdminDetails = getAdminDetails(updatedAdminDetails.getUserId());
		if (existingAdminDetails == null)
			throw new AdminDetailException(ADMIN_NOT_FOUND);

		if ((updatedAdminDetails.getUserName() != null
				&& updatedAdminDetails.getUserName().equals(existingAdminDetails.getUserName()))
				&& (updatedAdminDetails.getEmailId() != null
						&& updatedAdminDetails.getEmailId().equals(existingAdminDetails.getEmailId())
						&& (updatedAdminDetails.getMobileNumber() != null && updatedAdminDetails.getMobileNumber()
								.equals(existingAdminDetails.getMobileNumber())))

		) {
			throw new CustomerDetailException(NO_RECORDS_CHANGED);

		}

		if (updatedAdminDetails.getUserName() != null && !updatedAdminDetails.getUserName().equals("")) {
			existingAdminDetails.setUserName(updatedAdminDetails.getUserName());
		}
		if (updatedAdminDetails.getEmailId() != null && !updatedAdminDetails.getEmailId().equals("")) {
			if (!updatedAdminDetails.getEmailId().equals(existingAdminDetails.getEmailId())) {
				isEmailAddressUpdated = true;
			}
			existingAdminDetails.setEmailId(updatedAdminDetails.getEmailId());
		}
		if (updatedAdminDetails.getMobileNumber() != null && !updatedAdminDetails.getMobileNumber().equals("")) {
			existingAdminDetails.setMobileNumber(updatedAdminDetails.getMobileNumber());
		}
		if (updatedAdminDetails.getRole() != null && !updatedAdminDetails.getRole().equals("")) {
			existingAdminDetails.setRole(updatedAdminDetails.getRole());
		}

		existingAdminDetails.setUpdatedDate(commonUtils.getDate());
		adminDetails = adminAuthenticationRepository.save(existingAdminDetails);

		if (adminDetails.getUserId() != null) {
			AdminDetails.Response response = new AdminDetails().new Response();
			response.setCode(200);
			response.setMessage(ADMIN_UPDATED_MSG);
			response.setStatus(SUCCESS);
			response.setAdminDetails(adminDetails);
			if (isEmailAddressUpdated) {
				final UserDetails userDetails = userDetailsService
						.loadUserByUsername(adminDetails.getEmailId() + ":Admin");

				final String token = jwtTokenUtil.generateToken(userDetails, "Admin");
				response.setJwttoken(token);
				isEmailAddressUpdated = false;
			}
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End of updateAdminDetails");
		return responseEntity;

	}

	/**
	 * This method is used to get the admin details based on the email Id.
	 * 
	 * @param emailId
	 * @return AdminDetails
	 */
	public AdminDetails fetchUserByEmailId(String emailId) {
		LOGGER.info("Inside of fetchUserByEmailId");
		AdminDetails adminDetails = adminAuthenticationRepository.findByUsername(emailId);
		if (adminDetails != null) {
			LOGGER.info("End of fetchUserByEmailId");
			return adminDetails;
		} else {
			LOGGER.info("End of fetchUserByEmailId");
			return null;
		}
	}

	/**
	 * This method is used to validate the login details and get the admin details
	 * from database.
	 * 
	 * @param emailId
	 * @return AdminDetails
	 */
	public ResponseEntity<?> validateAdmin(String emailId) {
		LOGGER.info("Inside of validateAdmin");
		ResponseEntity<Object> responseEntity = null;

		if (emailId == null || emailId.equals("")) {
			throw new CustomerDetailException(EMAIL_ID_REQUIRED);
		}
		AdminDetails adminDetails = fetchByUsername(emailId);
		if (adminDetails == null) {
			throw new AdminDetailException(ADMIN_NOT_FOUND);
		} else {
			adminDetails.setPassword("");
			responseEntity = new ResponseEntity<>(adminDetails, HttpStatus.OK);

		}
		LOGGER.info("End of validateAdmin");
		return responseEntity;
	}

	/**
	 * This method is used to get the admin details from database based on userid.
	 * 
	 * @param userId
	 * @return AdminDetails
	 */
	public ResponseEntity<?> getAdminDetailsByUserId(String userId) {
		LOGGER.info("Inside of getAdminDetailsByUserId");
		ResponseEntity<Object> responseEntity = null;
		AdminDetails adminDetails = getAdminDetails(userId);
		if (adminDetails == null) {
			throw new AdminDetailException(ADMIN_NOT_FOUND);
		} else {
			adminDetails.setPassword("");
			responseEntity = new ResponseEntity<>(adminDetails, HttpStatus.OK);
		}
		LOGGER.info("End of getAdminDetailsByUserId");
		return responseEntity;
	}

	/**
	 * This method is used to get the admin details from database based on userid.
	 * 
	 * @param userId
	 * @return AdminDetails
	 */
	public AdminDetails getAdminDetails(String userId) {
		LOGGER.info("Inside of getAdminDetails");
		Optional<AdminDetails> adminDetails = adminAuthenticationRepository.findById(userId);
		if (adminDetails.isPresent()) {
			LOGGER.info("End of getAdminDetails");
			return adminDetails.get();
		} else {
			LOGGER.info("End of getAdminDetails");
			return null;
		}

	}

	/**
	 * This method is used to get all admin details from database based on userid.
	 * 
	 * @param userId
	 * @return AdminDetails
	 */
	public ResponseEntity<?> getAllAdminDetails() {
		LOGGER.info("Inside of getAllAdminDetails");
		List<AdminDetails> adminDetails = adminAuthenticationRepository.findAll();
		if (adminDetails.size() > 0) {
			LOGGER.info("End of getAdminDetails");
			return new ResponseEntity<>(adminDetails, HttpStatus.OK);

		} else {
			LOGGER.info("End of getAdminDetails");
			throw new AdminDetailException(NO_RECORDS_FOUND);
		}

	}

	/**
	 * This methods deletes the user from database.
	 * 
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> deleteByUserId(String userId) {
		LOGGER.info("Inside of deleteByUserId");
		ResponseEntity<Object> responseEntity = null;

		if (userId == null || userId.equals("")) {
			throw new AdminDetailException(USER_ID_REQUIRED);
		}
		if (getAdminDetails(userId) == null) {

			throw new AdminDetailException(ADMIN_NOT_FOUND);
		}
		adminAuthenticationRepository.deleteById(userId);

		if (getAdminDetails(userId) == null) {
			AdminDetails.Response response = new AdminDetails().new Response();
			response.setCode(200);
			response.setMessage(ADMIN_DELETED_MSG);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End of deleteByUserId");
		return responseEntity;

	}

}
