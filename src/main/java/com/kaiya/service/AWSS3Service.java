package com.kaiya.service;

import org.springframework.web.multipart.MultipartFile;

import com.kaiya.model.AwsImageUpload;

public interface AWSS3Service {

	AwsImageUpload uploadFile(MultipartFile multipartFile,String path);

	void deleteFile(final String keyName);
}
