package com.kaiya.service;

import static com.kaiya.api.Constants.NO_RECORDS_CHANGED;
import static com.kaiya.api.Constants.PRODUCT_ADDED_MSG;
import static com.kaiya.api.Constants.PRODUCT_DELETED_MSG;
import static com.kaiya.api.Constants.PRODUCT_EXIST_ERR_MSG;
import static com.kaiya.api.Constants.PRODUCT_ID_PREFIX;
import static com.kaiya.api.Constants.PRODUCT_NOT_AVAILABLE;
import static com.kaiya.api.Constants.PRODUCT_UPDATED_MSG;
import static com.kaiya.api.Constants.SUCCESS;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaiya.dao.CartDetailsRepository;
import com.kaiya.dao.ProductDetailsRepository;
import com.kaiya.entity.CartDetails;
import com.kaiya.entity.InvoiceDetails;
import com.kaiya.entity.ProductDetails;
import com.kaiya.exceptions.ProductDetailException;
import com.kaiya.model.AwsImageUpload;
import com.kaiya.util.CommonUtils;

@Service
public class ProductService {

	@Autowired
	private ProductDetailsRepository productDetailsRepository;

	@Autowired
	private CartDetailsRepository cartDetailsRepository;

	@Autowired
	private CommonUtils commonUtils;

	@Autowired
	private AWSS3ServiceImpl awsS3Service;

	@Autowired
	private CartService cartService;

	public ProductDetails getJson(String productDetails) {
		ProductDetails product = new ProductDetails();

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			// objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
			// false);
			product = objectMapper.readValue(productDetails, ProductDetails.class);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return product;

	}

	/**
	 * This methods gets the product from database and returns the list of product
	 * details.
	 * 
	 * @return Array of product details
	 */
	public List<ProductDetails> getProductDetails(String customerId) {

		List<CartDetails> cartDetails = cartDetailsRepository.findByCustomerId(customerId);
		Map<String, ProductDetails> products = productDetailsRepository.findAllMap();

		List<ProductDetails> productDetails = null;

		cartDetails.forEach(cartProducts -> {
			String productId = cartProducts.getProductDetails().getProductId();

			ProductDetails productDetail = products.get(productId);
			productDetail.setQuantity(cartProducts.getQuantity());
			productDetail.setIsCartClicked(true);
			productDetail.setCartId(cartProducts.getCartId());

		});
		productDetails = products.values().stream().collect(Collectors.toList());

		return productDetails;

	}

	/**
	 * This methods gets all product from database and returns the list of product
	 * details.
	 * 
	 * @return Array of product details
	 */
	public List<ProductDetails> getAllProductDetails() {

		return productDetailsRepository.findAll();

	}

	/**
	 * This methods adds the product to database.
	 * 
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> addProduct(ProductDetails newProductDetail, MultipartFile file) {

		ResponseEntity<Object> responseEntity = null;

		ProductDetails updatedProductDetails = null;
		AwsImageUpload awsImage = awsS3Service.uploadFile(file, "Products");
		if (newProductDetail.getProductName().equals("") || newProductDetail.getProductName() == null) {
			throw new ProductDetailException("Product name is required");
		} else if (newProductDetail.getAmount().equals("") || newProductDetail.getAmount() == null) {
			throw new ProductDetailException("Amount is required");
		}
		/*
		 * if (fetchByProductName(newProductDetail.getProductName()) != null) {
		 * 
		 * throw new ProductDetailException(PRODUCT_EXIST_ERR_MSG); }
		 */

		newProductDetail.setProductId(PRODUCT_ID_PREFIX + commonUtils.getRandomNumberString());
		newProductDetail.setProductImage(awsImage.getImageUrl());
		newProductDetail.setProductImageKey(awsImage.getKey());
		try {
			updatedProductDetails = productDetailsRepository.save(newProductDetail);
		} catch (Exception exception) {
			throw new ProductDetailException(exception.getMessage());
		}

		if (updatedProductDetails != null) {
			ProductDetails.Response response = new ProductDetails().new Response();
			response.setCode(200);
			response.setMessage(PRODUCT_ADDED_MSG);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}

		return responseEntity;

	}

	/**
	 * This methods updates the product to database.
	 * 
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> updateProduct(ProductDetails updatedProductDetail, MultipartFile file) {
		ResponseEntity<Object> responseEntity = null;
		AwsImageUpload awsImage = null;
		if (updatedProductDetail.isImageChanged()) {
			awsS3Service.deleteFile(updatedProductDetail.getProductImageKey());
			awsImage = awsS3Service.uploadFile(file, "Products");
		}

		if (updatedProductDetail.getProductId() == null || updatedProductDetail.getProductId().equals("")) {
			throw new ProductDetailException("Product Id is required");
		}
		ProductDetails oldProductDetails = fetchByProductId(updatedProductDetail.getProductId());
		if (oldProductDetails == null) {

			throw new ProductDetailException(PRODUCT_NOT_AVAILABLE);
		}

		if (oldProductDetails != null
				&& (updatedProductDetail.getProductName() != null
						&& oldProductDetails.getProductName().equals(updatedProductDetail.getProductName()))
				&& (updatedProductDetail.getAmount() != null
						&& oldProductDetails.getAmount().equals(updatedProductDetail.getAmount()))
				&& (updatedProductDetail.getOfferAmount() != null
						&& oldProductDetails.getOfferAmount().equals(updatedProductDetail.getOfferAmount()))
				&& (updatedProductDetail.getLitres() != null
						&& oldProductDetails.getLitres().equals(updatedProductDetail.getLitres()))
				&& updatedProductDetail.getMin() == oldProductDetails.getMin()
				&& updatedProductDetail.getMax() == oldProductDetails.getMax()
				&& (updatedProductDetail.getStatus() != null
						&& oldProductDetails.getStatus().equals(updatedProductDetail.getStatus()))
				&& !updatedProductDetail.isImageChanged()) {
			throw new ProductDetailException(NO_RECORDS_CHANGED);
		}

		if (updatedProductDetail.getProductName() != null
				&& (!oldProductDetails.getProductName().equals(updatedProductDetail.getProductName()))) {
			oldProductDetails.setProductName(updatedProductDetail.getProductName());
		}
		if (updatedProductDetail.getAmount() != null
				&& (!oldProductDetails.getAmount().equals(updatedProductDetail.getAmount()))) {
			oldProductDetails.setAmount(updatedProductDetail.getAmount());
		}
		if (updatedProductDetail.getOfferAmount() != null
				&& (!oldProductDetails.getOfferAmount().equals(updatedProductDetail.getOfferAmount()))) {
			oldProductDetails.setOfferAmount(updatedProductDetail.getOfferAmount());
		}
		if (updatedProductDetail.getLitres() != null
				&& (!oldProductDetails.getLitres().equals(updatedProductDetail.getLitres()))) {
			oldProductDetails.setLitres(updatedProductDetail.getLitres());
		}
		if (updatedProductDetail.getStatus() != null
				&& (!oldProductDetails.getStatus().equals(updatedProductDetail.getStatus()))) {
			oldProductDetails.setStatus(updatedProductDetail.getStatus());
		}

		if (updatedProductDetail.getMin() != oldProductDetails.getMin()) {
			oldProductDetails.setMin(updatedProductDetail.getMin());
		}
		if (updatedProductDetail.getMax() != oldProductDetails.getMax()) {
			oldProductDetails.setMax(updatedProductDetail.getMax());
		}
		if (updatedProductDetail.isImageChanged()) {
			oldProductDetails.setProductImageKey(awsImage.getKey());
			oldProductDetails.setProductImage(awsImage.getImageUrl());
		}

		updatedProductDetail = productDetailsRepository.save(oldProductDetails);

		if (updatedProductDetail != null) {
			ProductDetails.Response response = new ProductDetails().new Response();
			response.setCode(200);
			response.setMessage(PRODUCT_UPDATED_MSG);
			response.setStatus(SUCCESS);

			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}

		return responseEntity;

	}

	/**
	 * This methods deletes the product from database.
	 * 
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> deleteByProductId(String productId) {
		ResponseEntity<Object> responseEntity = null;

		if (productId == null || productId.equals("")) {
			throw new ProductDetailException("Product Id is required");
		}
		if (fetchByProductId(productId) == null) {

			throw new ProductDetailException(PRODUCT_NOT_AVAILABLE);
		}
		cartService.deleteCartDetailsByProductId(productId);
		productDetailsRepository.deleteById(productId);

		if (fetchByProductId(productId) == null) {
			ProductDetails.Response response = new ProductDetails().new Response();
			response.setCode(200);
			response.setMessage(PRODUCT_DELETED_MSG);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}

		return responseEntity;

	}

	/**
	 * This methods gets the product from database by productId and returns the
	 * product details.
	 * 
	 * @return product details
	 */
	public ProductDetails fetchByProductId(String productId) {
		Optional<ProductDetails> productDetail;
		productDetail = productDetailsRepository.findById(productId);

		if (productDetail.isPresent()) {

			return productDetail.get();
		} else {
			return null;

		}

	}

	/**
	 * This methods gets the product from database by productName and returns the
	 * product details.
	 * 
	 * @return product details
	 */
	public ProductDetails fetchByProductName(String productName) {

		return productDetailsRepository.findByProductName(productName);

	}

}
