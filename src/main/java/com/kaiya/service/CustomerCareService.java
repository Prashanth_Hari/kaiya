package com.kaiya.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.kaiya.controller.CustomerCareController;
import com.kaiya.dao.CustomerCareRepository;
import com.kaiya.entity.CustomerCareDetails;
import com.kaiya.exceptions.AdminDetailException;

@Service
public class CustomerCareService {

	private static Logger LOGGER = LogManager.getLogger(CustomerCareService.class);

	@Autowired
	private CustomerCareRepository customerCareRepository;

	/**
	 * This methods gets the customer care details from database.
	 * 
	 * @return response entity
	 */
	public ResponseEntity<?> getCustomerCareDetails() {
		LOGGER.info("Inside get customer care service");

		List<CustomerCareDetails> customerCareDetails = customerCareRepository.findAll();

		if (customerCareDetails.size() == 0) {
			LOGGER.error("Admin Details are empty");

			throw new AdminDetailException("Admin Details are empty");
		} else {
			LOGGER.info("Customer Care Details Fetched successfully");

			return new ResponseEntity<>(customerCareDetails.get(0), HttpStatus.OK);

		}
	}
}
