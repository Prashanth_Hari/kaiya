package com.kaiya.service;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.kaiya.api.JwtTokenUtil;
import com.kaiya.dao.CustomerDetailsRepository;
import com.kaiya.entity.CustomerDetails;
import com.kaiya.exceptions.CustomerDetailException;
import com.kaiya.model.Changepassword;
import com.kaiya.model.ResetPassword;
import com.kaiya.util.CommonUtils;
import static com.kaiya.api.Constants.EMAIL_ID_REQUIRED;
import static com.kaiya.api.Constants.PASSWORD_REQUIRED;
import static com.kaiya.api.Constants.NEW_PASSWORD_REQUIRED;
import static com.kaiya.api.Constants.CONFIRM_PASSWORD_REQUIRED;

import static com.kaiya.api.Constants.PASSWORD_CHANGED;
import static com.kaiya.api.Constants.MOBILENUMBER_ALREADY_EXIST;
import static com.kaiya.api.Constants.CUSTOMER_CREATED_MSG;
import static com.kaiya.api.Constants.SUCCESS;
import static com.kaiya.api.Constants.EMAIL_ALREADY_EXIST;
import static com.kaiya.api.Constants.GST_ALREADY_EXIST;
import static com.kaiya.api.Constants.EMAIL_SENT_SUCCESSFULLY;
import static com.kaiya.api.Constants.CUSTOMER_NOT_FOUND;
import static com.kaiya.api.Constants.NO_RECORDS_CHANGED;
import static com.kaiya.api.Constants.CUSTOMER_ID_MSG;
import static com.kaiya.api.Constants.CUSTOMER_UPDATED_MSG;
import static com.kaiya.api.Constants.CUSTOMER_ROLE;
import static com.kaiya.api.Constants.OTP_VALID_MSG;
import static com.kaiya.api.Constants.OTP_INVALID_MSG;
import static com.kaiya.api.Constants.EMAIL_DOSENT_EXIST;

@Service
public class CustomerService {

	private static Logger LOGGER = LogManager.getLogger(CustomerService.class);

	@Autowired
	private CustomerDetailsRepository customerDetailsRepository;

	@Autowired
	private CommonUtils commonUtils;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private EmailServiceImpl emailServiceImpl;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService userDetailsService;

	public ResponseEntity<?> saveCustomer(CustomerDetails customerDetails) {
		LOGGER.info("Inside saveCustomer");
		ResponseEntity<Object> responseEntity = null;
		CustomerDetails customer = null;
		try {
			if (customerDetails.getEmailId() == null || customerDetails.getEmailId().equals("")) {
				throw new CustomerDetailException("EmailId is required");
			} else if (customerDetails.getMobileNumber() == null || customerDetails.getMobileNumber().equals("")) {
				throw new CustomerDetailException("Mobile Number is required");
			} else if (customerDetails.getPassword() == null || customerDetails.getPassword().equals("")) {
				throw new CustomerDetailException("Password is required");
			}

			if (fetchUserByEmailId(customerDetails.getEmailId()) != null)
				throw new CustomerDetailException(EMAIL_ALREADY_EXIST);

			if (fetchUserByMobileNumber(customerDetails.getMobileNumber()) != null) {
				throw new CustomerDetailException(MOBILENUMBER_ALREADY_EXIST);

			}

			if (customerDetails.getGst_Id() != null && customerDetails.getGst_Id() != "") {
				if (fetchUserByGstId(customerDetails.getGst_Id()) != null) {
					throw new CustomerDetailException(GST_ALREADY_EXIST);

				}
			}
			String encryptedPwd = bCryptPasswordEncoder.encode(customerDetails.getPassword());
			customerDetails.setCustomerId("CST" + commonUtils.getRandomNumberString());

			customerDetails.setCreatedDate(commonUtils.getDate());
			customerDetails.setUpdatedDate(commonUtils.getDate());
			customerDetails.setRole(CUSTOMER_ROLE);
			customerDetails.setPassword(encryptedPwd);
			customer = customerDetailsRepository.save(customerDetails);

		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Inside create customer" + e);
		}
		if (customer.getCustomerId() != null) {
			CustomerDetails.Response response = new CustomerDetails().new Response();
			response.setCode(200);
			response.setMessage(CUSTOMER_CREATED_MSG);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End saveCustomer");
		return responseEntity;

	}

	/**
	 * This method is used to get the customer details based on the email Id.
	 * 
	 * @param emailId
	 * @return CustomerDetails
	 */
	public CustomerDetails fetchUserByEmailId(String emailId) {
		LOGGER.info("Inside fetchUserByEmailId");
		Optional<CustomerDetails> customerDetails = customerDetailsRepository.findByEmailId(emailId);
		if (customerDetails.isPresent()) {
			LOGGER.info("End fetchUserByEmailId");
			return customerDetails.get();
		} else {
			LOGGER.info("End fetchUserByEmailId");
			return null;
		}
	}

	/**
	 * This method is used to get the customer details based on the mobile number.
	 * 
	 * @param emailId
	 * @return CustomerDetails
	 */
	public CustomerDetails fetchUserByMobileNumber(String mobileNumber) {
		LOGGER.info("Inside fetchUserByMobileNumber");
		Optional<CustomerDetails> customerDetails = customerDetailsRepository.findByPhoneNumber(mobileNumber);
		if (customerDetails.isPresent()) {
			LOGGER.info("End fetchUserByMobileNumber");
			return customerDetails.get();
		} else {
			LOGGER.info("End fetchUserByMobileNumber");
			return null;
		}
	}

	public CustomerDetails fetchUserByGstId(String gstId) {
		LOGGER.info("Inside fetchUserByGstId");
		return customerDetailsRepository.findByGstId(gstId);

	}

	/**
	 * This method is used to change the password and update the database.
	 * 
	 * @param changePassword
	 * @return
	 */
	public ResponseEntity<?> changePassword(Changepassword changePassword) {
		LOGGER.info("Inside changePassword");
		ResponseEntity<Object> responseEntity = null;
		CustomerDetails customer = null;
		if (changePassword.getCustomerId() == null || changePassword.getCustomerId().equals("")) {
			throw new CustomerDetailException(CUSTOMER_ID_MSG);
		} else if (changePassword.getPassword() == null || changePassword.getPassword().equals("")) {
			throw new CustomerDetailException(PASSWORD_REQUIRED);
		} else if (changePassword.getNewPassword() == null || changePassword.getNewPassword().equals("")) {
			throw new CustomerDetailException(NEW_PASSWORD_REQUIRED);
		} else if (changePassword.getConfirmPassword() == null || changePassword.getConfirmPassword().equals("")) {
			throw new CustomerDetailException(CONFIRM_PASSWORD_REQUIRED);
		}
		CustomerDetails existingCustomer = getCustomerDetails(changePassword.getCustomerId());
		if (existingCustomer == null) {
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		}
		try {
			if (!bCryptPasswordEncoder.matches(changePassword.getPassword(), existingCustomer.getPassword())) {
				throw new CustomerDetailException("Current password is invalid.");
			} else if (bCryptPasswordEncoder.matches(changePassword.getNewPassword(), existingCustomer.getPassword())) {
				throw new CustomerDetailException("Current password and New password should not be same.");

			} else {

				existingCustomer.setPassword(bCryptPasswordEncoder.encode(changePassword.getNewPassword()));
				customer = customerDetailsRepository.save(existingCustomer);

			}

		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Inside Change password" + e);
		}
		if (getCustomerDetails(customer.getCustomerId()) != null) {
			CustomerDetails.Response response = new CustomerDetails().new Response();
			response.setCode(200);
			response.setMessage(PASSWORD_CHANGED);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End changePassword");
		return responseEntity;

	}

	/**
	 * This method is used to reset the password and updates the new password to
	 * database.
	 * 
	 * @param changePassword
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> resetPassword(ResetPassword resetPassword) {
		LOGGER.info("Inside resetPassword");
		ResponseEntity<Object> responseEntity = null;
		CustomerDetails customer = null;
		if (resetPassword.getEmailId() == null || resetPassword.getEmailId().equals("")) {
			throw new CustomerDetailException(EMAIL_ID_REQUIRED);
		} else if (resetPassword.getNewPassword() == null || resetPassword.getNewPassword().equals("")) {
			throw new CustomerDetailException(NEW_PASSWORD_REQUIRED);
		}
		CustomerDetails existingCustomer = fetchUserByEmailId(resetPassword.getEmailId());
		if (existingCustomer == null) {
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		}
		try {
			existingCustomer.setPassword(bCryptPasswordEncoder.encode(resetPassword.getNewPassword()));
			customer = customerDetailsRepository.save(existingCustomer);

		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Inside Change password" + e);
		}
		if (getCustomerDetails(customer.getCustomerId()) != null) {
			CustomerDetails.Response response = new CustomerDetails().new Response();
			response.setCode(200);
			response.setMessage(PASSWORD_CHANGED);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End resetPassword");
		return responseEntity;

	}

	/**
	 * This method is used to update the customer details to the database.
	 * 
	 * @param customerDetails
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> updateCustomerDetails(CustomerDetails customerDetails) {
		LOGGER.info("Inside updateCustomerDetails");
		ResponseEntity<Object> responseEntity = null;
		CustomerDetails customer = null;
		boolean isEmailAddressUpdated = false;
		if (customerDetails.getCustomerId() == null)
			throw new CustomerDetailException(CUSTOMER_ID_MSG);
		CustomerDetails existingCustomer = getCustomerDetails(customerDetails.getCustomerId());
		if (existingCustomer == null)
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);

		if (customerDetails.getCustomerName() == null && customerDetails.getEmailId() == null
				&& customerDetails.getMobileNumber() == null && customerDetails.getGst_Id() == null)
			throw new CustomerDetailException(NO_RECORDS_CHANGED);

		if ((customerDetails.getCustomerName() != null
				&& customerDetails.getCustomerName().equals(existingCustomer.getCustomerName()))
				&& (customerDetails.getEmailId() != null
						&& customerDetails.getEmailId().equals(existingCustomer.getEmailId()))
				&& (customerDetails.getMobileNumber() != null
						&& customerDetails.getMobileNumber().equals(existingCustomer.getMobileNumber()))
				&& (customerDetails.getGst_Id() != null
						&& customerDetails.getGst_Id().equals(existingCustomer.getGst_Id()))
				&& (customerDetails.getCompanyName() != null
						&& customerDetails.getCompanyName().equals(existingCustomer.getCompanyName()))) {
			throw new CustomerDetailException(NO_RECORDS_CHANGED);

		}

		if (customerDetails.getCustomerName() == null) {
			existingCustomer.setCustomerName(existingCustomer.getCustomerName());
		} else {
			existingCustomer.setCustomerName(customerDetails.getCustomerName());

		}
		if (customerDetails.getEmailId() == null) {
			existingCustomer.setEmailId(existingCustomer.getEmailId());
		} else {
			if (!customerDetails.getEmailId().equals(existingCustomer.getEmailId())) {
				isEmailAddressUpdated = true;
			}
			existingCustomer.setEmailId(customerDetails.getEmailId());

		}

		if (customerDetails.getMobileNumber() == null) {
			existingCustomer.setMobileNumber(existingCustomer.getMobileNumber());
		} else {
			existingCustomer.setMobileNumber(customerDetails.getMobileNumber());

		}
		if (customerDetails.getGst_Id() == null) {
			existingCustomer.setGst_Id(existingCustomer.getGst_Id());
		} else {
			existingCustomer.setGst_Id(customerDetails.getGst_Id());

		}
		if (customerDetails.getCompanyName() == null) {
			existingCustomer.setCompanyName(existingCustomer.getCompanyName());
		} else {
			existingCustomer.setCompanyName(customerDetails.getCompanyName());

		}
		existingCustomer.setUpdatedDate(commonUtils.getDate());
		customer = customerDetailsRepository.save(existingCustomer);
		customer.setPassword("");
		if (customer.getCustomerId() != null) {
			CustomerDetails.Response response = new CustomerDetails().new Response();
			response.setCode(200);
			response.setMessage(CUSTOMER_UPDATED_MSG);
			response.setStatus(SUCCESS);
			response.setCustomerDetails(customer);
			if (isEmailAddressUpdated) {
				final UserDetails userDetails = userDetailsService
						.loadUserByUsername(customerDetails.getEmailId() + ":Customer");

				final String token = jwtTokenUtil.generateToken(userDetails, "Customer");
				response.setJwttoken(token);
				isEmailAddressUpdated = false;
			}
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End updateCustomerDetails");
		return responseEntity;

	}

	/**
	 * This method is used to validate the login details and get the customers from
	 * database.
	 * 
	 * @param emailId
	 * @return CustomerDetails
	 */
	public CustomerDetails validateLogin(String emailId) {
		LOGGER.info("Start validateLogin");
		if (emailId == null || emailId.equals("")) {
			throw new CustomerDetailException(EMAIL_ID_REQUIRED);
		}
		CustomerDetails customerDetail = fetchUserByEmailId(emailId);
		if (customerDetail == null) {
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		} else {
			customerDetail.setLastLogin(commonUtils.getDate());
			customerDetailsRepository.save(customerDetail);
		}

		customerDetail.setPassword("");
		LOGGER.info("End validateLogin");
		return customerDetail;
	}

	/**
	 * This method is used to get the customer details based on the customerid.
	 * 
	 * @param customerId
	 * @return CustomerDetails
	 */
	public CustomerDetails getCustomerDetails(String customerId) {
		LOGGER.info("Inside getCustomerDetails");
		Optional<CustomerDetails> customerDetails = customerDetailsRepository.findById(customerId);
		if (customerDetails.isPresent()) {
			LOGGER.info("End getCustomerDetails");
			return customerDetails.get();
		} else {
			LOGGER.info("End getCustomerDetails");
			return null;
		}

	}

	/**
	 * This method is used to get all customer details from database.
	 */
	public ResponseEntity<?> getAllCustomerDetails() {
		LOGGER.info("Inside getAllCustomerDetails");
		List<CustomerDetails> customerDetails = customerDetailsRepository.findAll();
		if (customerDetails.size() > 0) {
			LOGGER.info("End getAllCustomerDetails");
			return new ResponseEntity<>(customerDetails, HttpStatus.OK);
		} else {
			LOGGER.info("End getAllCustomerDetails");
			return null;
		}

	}

	/**
	 * This method is used to send the otp based on provided emailId.
	 * 
	 * @param resetPassword
	 * @return
	 */
	public ResponseEntity<?> sendResetPasswordOtp(CustomerDetails resetPassword) {
		LOGGER.info("Inside sendResetPasswordOtp");
		ResponseEntity<Object> responseEntity = null;
		CustomerDetails customer = null;
		try {
			if (resetPassword.getEmailId() == null || resetPassword.getEmailId().equals("")) {
				throw new CustomerDetailException("EmailId is required");
			}
			CustomerDetails existingCustomer = fetchUserByEmailId(resetPassword.getEmailId());
			if (existingCustomer == null) {
				throw new CustomerDetailException(EMAIL_DOSENT_EXIST);
			}
			existingCustomer.setOtp(generateOTP());
			customer = customerDetailsRepository.save(existingCustomer);
			emailServiceImpl.sendForgotPasswordLink(existingCustomer, null);

			if (customer != null) {
				CustomerDetails.Response response = new CustomerDetails().new Response();
				response.setCode(200);
				response.setMessage(EMAIL_SENT_SUCCESSFULLY);
				response.setStatus(SUCCESS);
				responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
			}

		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Inside create customer" + e);
		}
		LOGGER.info("End sendResetPasswordOtp");
		return responseEntity;

	}

	/**
	 * This method is used to validate the otp.
	 * 
	 * @param cutomerDetails
	 * @return
	 */
	public ResponseEntity<?> validateOTP(CustomerDetails cutomerDetails) {
		LOGGER.info("Inside validateOTP");
		ResponseEntity<Object> responseEntity = null;
		try {
			if (cutomerDetails.getOtp() == null || cutomerDetails.getOtp().equals("")) {
				throw new CustomerDetailException("OTP is required");
			}
			CustomerDetails existingCustomer = fetchUserByEmailId(cutomerDetails.getEmailId());
			if (existingCustomer == null) {
				throw new CustomerDetailException(EMAIL_DOSENT_EXIST);
			}

			if (cutomerDetails.getOtp().equals(existingCustomer.getOtp())) {
				existingCustomer.setOtp(null);
				if (existingCustomer != null) {
					existingCustomer = customerDetailsRepository.save(existingCustomer);
					CustomerDetails.Response response = new CustomerDetails().new Response();
					response.setCode(200);
					response.setMessage(OTP_VALID_MSG);
					response.setStatus(SUCCESS);
					responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
				}
			} else {
				throw new CustomerDetailException(OTP_INVALID_MSG);
			}

		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Inside create customer" + e);
		}
		LOGGER.info("End validateOTP");
		return responseEntity;

	}

	/**
	 * This method is used to generate the 4 digit otp number.
	 * 
	 * @return otp
	 */

	public static String generateOTP() {
		return String.valueOf((int) (Math.random() * 9000) + 1000);
	}

}
