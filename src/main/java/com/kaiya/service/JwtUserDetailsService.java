package com.kaiya.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kaiya.dao.AdminAuthenticationRepository;
import com.kaiya.dao.UserAuthenticationRepository;
import com.kaiya.entity.AdminDetails;
import com.kaiya.entity.JWTUserAuthentication;
import com.kaiya.exceptions.CustomerDetailException;
import static com.kaiya.api.Constants.CUSTOMER_NOT_FOUND;
import static com.kaiya.api.Constants.USER_NOT_FOUND;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	@Autowired
	private UserAuthenticationRepository userAuthenticationRepository;

	@Autowired
	private AdminAuthenticationRepository adminAuthenticationRepository;

	JWTUserAuthentication user;

	AdminDetails adminDetails;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		String[] splittedUsername = username.split(":");
		if (splittedUsername[1].equals("Admin")) {
			adminDetails = adminAuthenticationRepository.findByUsername(splittedUsername[0]);
			if (adminDetails == null) {
				throw new CustomerDetailException(USER_NOT_FOUND);
			}
			authorities.add(new SimpleGrantedAuthority(adminDetails.getRole()));

			return new org.springframework.security.core.userdetails.User(adminDetails.getEmailId(),
					adminDetails.getPassword(), authorities);

		} else if (splittedUsername[1].equals("Customer")) {
			user = userAuthenticationRepository.findByUsername(splittedUsername[0]);
			if (user == null) {
				throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
			}
			authorities.add(new SimpleGrantedAuthority(user.getRole()));
			return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
					authorities);
		} else {
			throw new UsernameNotFoundException("User not found" + username);

		}
	}

}