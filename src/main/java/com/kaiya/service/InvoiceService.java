package com.kaiya.service;

import static com.kaiya.api.Constants.ADMIN_ROLE;
import static com.kaiya.api.Constants.CUSTOMER_NOT_FOUND;
import static com.kaiya.api.Constants.INVOICE_CREATED_MSG;
import static com.kaiya.api.Constants.INVOICE_ID_ERR_MSG;
import static com.kaiya.api.Constants.INVOICE_NOT_FOUND;
import static com.kaiya.api.Constants.SUCCESS;
import static com.kaiya.api.Constants.CUSTOMER_ID_REQUIRED;
import static com.kaiya.api.Constants.COMMENTS_REQUIRED;
import static com.kaiya.api.Constants.INVOICE_ALREADY_EXIST;
import static com.kaiya.api.Constants.INVOICE_UPDATED_MSG;

import java.io.File;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaiya.dao.InvoiceRepository;
import com.kaiya.dao.OrdersRepository;
import com.kaiya.entity.CustomerDetails;
import com.kaiya.entity.InvoiceDetails;
import com.kaiya.entity.Orders;
import com.kaiya.exceptions.CustomerDetailException;
import com.kaiya.exceptions.InvoiceDetailException;
import com.kaiya.model.AwsImageUpload;
import com.kaiya.util.CommonUtils;

@Service
public class InvoiceService {
	private static Logger LOGGER = LogManager.getLogger(InvoiceService.class);
	@Autowired
	private InvoiceRepository invoiceRepository;

	@Autowired
	private CommonUtils commonUtils;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private OrdersRepository orderRepository;

	@Autowired
	private AWSS3ServiceImpl awsS3Service;

	@Autowired
	private EmailServiceImpl emailServiceImpl;

	/**
	 * This methods gets all the invoice from database and returns the invoice
	 * details.
	 * 
	 * @return invoice details
	 */
	public ResponseEntity<?> getAllInvoice() {
		LOGGER.info("Start getAllInvoice");
		ResponseEntity<Object> responseEntity = null;

		Iterable<InvoiceDetails> invoiceDetails = invoiceRepository.findAll();
		responseEntity = new ResponseEntity<>(invoiceDetails, HttpStatus.OK);
		LOGGER.info("End getAllInvoice");
		return responseEntity;

	}

	/**
	 * This methods gets all the invoice from database by customer id and returns
	 * the invoice details.
	 * 
	 * @return invoice details
	 */
	public ResponseEntity<?> getInvoiceByCustomerId(String customerId) {
		LOGGER.info("Start getInvoiceByCustomerId");
		ResponseEntity<Object> responseEntity = null;

		if (customerService.getCustomerDetails(customerId) == null) {
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		}

		List<InvoiceDetails> invoiceDetails = invoiceRepository.findInvoiceByCustomerId(customerId);
		responseEntity = new ResponseEntity<>(invoiceDetails, HttpStatus.OK);
		LOGGER.info("End getInvoiceByCustomerId");
		return responseEntity;

	}

	public InvoiceDetails getJson(String invoiceDetails) {

		InvoiceDetails invoiceJson = new InvoiceDetails();

		try {
			ObjectMapper objectMapper = new ObjectMapper();

			invoiceJson = objectMapper.readValue(invoiceDetails, InvoiceDetails.class);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return invoiceJson;

	}

	/**
	 * This methods gets all the invoice from database by invoice id and returns the
	 * invoice details.
	 * 
	 * @return invoice details
	 */
	public ResponseEntity<?> getInvoiceByInvoiceId(String invoiceId) {
		LOGGER.info("Start getInvoiceByInvoiceId");
		ResponseEntity<Object> responseEntity = null;

		if (getInvoiceDetails(invoiceId) == null) {
			throw new InvoiceDetailException(INVOICE_NOT_FOUND);
		}

		Optional<InvoiceDetails> invoiceDetails = invoiceRepository.findById(invoiceId);
		responseEntity = new ResponseEntity<>(invoiceDetails, HttpStatus.OK);
		LOGGER.info("End getInvoiceByInvoiceId");
		return responseEntity;

	}

	/**
	 * This method is used to create the invoice and save it to the database.
	 * 
	 * @param invoiceDetails
	 * @return
	 */
	public ResponseEntity<?> createInvoice(InvoiceDetails invoiceDetails, MultipartFile file) {
		LOGGER.info("Start createInvoice");
		ResponseEntity<Object> responseEntity = null;
		CustomerDetails customerDetails;
		InvoiceDetails existingInvoiceDetails;
		existingInvoiceDetails = getInvoiceDetails(invoiceDetails.getInvoiceId());
		if (existingInvoiceDetails != null) {
			throw new InvoiceDetailException(INVOICE_ALREADY_EXIST);
		}
		AwsImageUpload awsImage = awsS3Service.uploadFile(file, "Invoice");

		if (invoiceDetails.getInvoiceId() == null || invoiceDetails.getInvoiceId().equals("")) {
			throw new InvoiceDetailException(INVOICE_ID_ERR_MSG);
		} else if (invoiceDetails.getCustomerId() == null || invoiceDetails.getCustomerId().equals("")) {
			throw new InvoiceDetailException(CUSTOMER_ID_REQUIRED);
		} else if (invoiceDetails.getComments() == null || invoiceDetails.getComments().equals("")) {
			throw new InvoiceDetailException(COMMENTS_REQUIRED);
		}
		invoiceDetails.setInvoiceImage(awsImage.getImageUrl());
		invoiceDetails.setInvoiceImageKey(awsImage.getKey());

		customerDetails = customerService.getCustomerDetails(invoiceDetails.getCustomerId());
		if (customerDetails == null) {
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		}

		invoiceDetails.setCreatedDate(commonUtils.getDate());
		invoiceDetails.setCreatedBy(ADMIN_ROLE);

		InvoiceDetails createdInvoiceDetails = invoiceRepository.save(invoiceDetails);
		Orders existingOrders = orderService.fetchOrderByOrderId(invoiceDetails.getOrderId());
		existingOrders.setInvoiceDetails(createdInvoiceDetails);
		orderRepository.save(existingOrders);
		//Disabling invoice email feature.
		//emailServiceImpl.sendInvoice(existingOrders, fileName);
		//File f = new File("./Images/" + file.getOriginalFilename());
		///f.delete();
		if (getInvoiceDetails(invoiceDetails.getInvoiceId()) != null) {
			InvoiceDetails.Response response = new InvoiceDetails().new Response();
			response.setCode(200);
			response.setMessage(INVOICE_CREATED_MSG);
			response.setStatus(SUCCESS);
			response.setData(createdInvoiceDetails);

			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End createInvoice");
		return responseEntity;

	}

	/**
	 * This method is used to update the invoice and save it to the database.
	 * 
	 * @param invoiceDetails
	 * @return
	 */
	public ResponseEntity<?> updateInvoice(InvoiceDetails invoiceDetails, MultipartFile file) {
		LOGGER.info("Start updateInvoice");
		ResponseEntity<Object> responseEntity = null;

		CustomerDetails customerDetails;
		AwsImageUpload awsImage = null;
		if (invoiceDetails.isImageChanged()) {
			awsS3Service.deleteFile(invoiceDetails.getInvoiceImageKey());
			awsImage = awsS3Service.uploadFile(file, "Invoice");
		}
		if (invoiceDetails.getInvoiceId() == null || invoiceDetails.getInvoiceId().equals("")) {
			throw new InvoiceDetailException(INVOICE_ID_ERR_MSG);
		}
		customerDetails = customerService.getCustomerDetails(invoiceDetails.getCustomerId());
		if (customerDetails == null) {
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		}

		InvoiceDetails existingInvoiceDetails = getInvoiceDetails(invoiceDetails.getInvoiceId());
		existingInvoiceDetails.setComments(invoiceDetails.getComments());
		existingInvoiceDetails.setUpdatedBy(commonUtils.getDate());
		existingInvoiceDetails.setUpdatedBy(ADMIN_ROLE);
		if (invoiceDetails.isImageChanged()) {
			existingInvoiceDetails.setInvoiceImageKey(awsImage.getKey());
			existingInvoiceDetails.setInvoiceImage(awsImage.getImageUrl());
		}
		InvoiceDetails createdInvoiceDetails = invoiceRepository.save(existingInvoiceDetails);
		if (getInvoiceDetails(invoiceDetails.getInvoiceId()) != null) {
			InvoiceDetails.Response response = new InvoiceDetails().new Response();
			response.setCode(200);
			response.setMessage(INVOICE_UPDATED_MSG);
			response.setStatus(SUCCESS);
			response.setData(createdInvoiceDetails);

			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End updateInvoice");
		return responseEntity;

	}

	/**
	 * This method is used to get the invoice details by invoice id.
	 * 
	 * @param invoiceId
	 * @return
	 */
	public InvoiceDetails getInvoiceDetails(String invoiceId) {
		LOGGER.info("Start getInvoiceDetails");
		Optional<InvoiceDetails> invoiceDetails = invoiceRepository.findById(invoiceId);

		if (invoiceDetails.isPresent()) {
			LOGGER.info("End getInvoiceDetails");
			return invoiceDetails.get();
		} else {
			LOGGER.info("End getInvoiceDetails");
			return null;
		}
	}
}
