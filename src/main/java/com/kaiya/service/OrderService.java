package com.kaiya.service;

import static com.kaiya.api.Constants.CUSTOMER_ID_REQUIRED;
import static com.kaiya.api.Constants.CUSTOMER_NOT_FOUND;
import static com.kaiya.api.Constants.NO_ORDER_EXIST;
import static com.kaiya.api.Constants.NO_RECORDS_FOUND;
import static com.kaiya.api.Constants.ORDER_CREATED;
import static com.kaiya.api.Constants.ORDER_CREATED_MSG;
import static com.kaiya.api.Constants.ORDER_DELETED_MSG;
import static com.kaiya.api.Constants.ORDER_DETAILID_PREFIX;
import static com.kaiya.api.Constants.ORDER_ID_PREFIX;
import static com.kaiya.api.Constants.ORDER_ID_REQUIRED;
import static com.kaiya.api.Constants.ORDER_UPDATED_MSG;
import static com.kaiya.api.Constants.PRODUCT_NOT_AVAILABLE;
import static com.kaiya.api.Constants.SUCCESS;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.kaiya.dao.OrderDetailsRepository;
import com.kaiya.dao.OrderStasticsRepository;
import com.kaiya.dao.OrdersRepository;
import com.kaiya.entity.CustomerDetails;
import com.kaiya.entity.OrderDetails;
import com.kaiya.entity.OrderStatstics;
import com.kaiya.entity.Orders;
import com.kaiya.entity.ProductDetails;
import com.kaiya.exceptions.CustomerDetailException;
import com.kaiya.exceptions.OrderDetailException;
import com.kaiya.exceptions.ProductDetailException;
import com.kaiya.model.OrderStatsticsCount;
import com.kaiya.util.CommonUtils;
import com.kaiya.util.OrderStatus;

@Service
public class OrderService {

	@Autowired
	private OrdersRepository orderRepository;

	@Autowired
	private OrderDetailsRepository orderDetailsRepository;

	@Autowired
	private OrderStasticsRepository orderStasticsRepository;

	@Autowired
	private CommonUtils commonUtils;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private ProductService productService;

	@Autowired
	private CartService cartService;

	@Autowired
	private InvoiceService invoiceSerive;

	@Autowired
	private EmailServiceImpl emailServiceImpl;

	/**
	 * This methods gets all the orders from database and returns the order details.
	 * 
	 * @return product details
	 */
	public ResponseEntity<?> getOrders() {

		List<Orders> order = orderRepository.findAllOrders();
		if (order.size() <= 0) {
			throw new OrderDetailException(NO_RECORDS_FOUND);
		} else {
			return new ResponseEntity<>(order, HttpStatus.OK);
		}

	}

	/**
	 * This methods returns the order details based on order id.
	 * 
	 * @return product details
	 */
	public ResponseEntity<?> getOrdersByOrderId(String orderId) {

		List<Orders> order = orderRepository.findOrdersByOrderId(orderId);
		if (order.size() <= 0) {
			throw new OrderDetailException(NO_RECORDS_FOUND);

		} else {
			return new ResponseEntity<>(order, HttpStatus.OK);

		}

	}

	/**
	 * This methods gets the customer id based on phone number and returns the order
	 * details.
	 * 
	 * @return product details
	 */
	public ResponseEntity<?> getOrdersByPhoneNumber(String phoneNumber) {

		CustomerDetails customerDetails = customerService.fetchUserByMobileNumber(phoneNumber);
		if (customerDetails == null) {
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		} else {

			return getOrder(customerDetails.getCustomerId());

		}

	}

	/**
	 * This methods gets the customer id based on email id and returns the order
	 * details.
	 * 
	 * @return product details
	 */
	public ResponseEntity<?> getOrdersByEmailId(String emaiId) {

		CustomerDetails customerDetails = customerService.fetchUserByEmailId(emaiId);
		if (customerDetails == null) {
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		} else {

			return getOrder(customerDetails.getCustomerId());

		}

	}

	/**
	 * This methods gets the order details based on date.
	 * 
	 * @return product details
	 */
	public ResponseEntity<?> getOrdersByDate(String date) {
		ResponseEntity<Object> responseEntity = null;
		List<Orders> orders = orderRepository.findOrdersByDate(date);
		if (orders.size() <= 0) {
			throw new OrderDetailException(NO_RECORDS_FOUND);
		} else {
			responseEntity = new ResponseEntity<>(orders, HttpStatus.OK);
			return responseEntity;
		}

	}

	/**
	 * This methods gets the order details based on date range.
	 * 
	 * @return product details
	 */
	public ResponseEntity<?> getOrdersByDateRange(String fromDate, String toDate) {
		ResponseEntity<Object> responseEntity = null;
		List<Orders> orders = orderRepository.findOrdersByDateRange(fromDate, toDate);
		if (orders.size() <= 0) {
			throw new OrderDetailException(NO_RECORDS_FOUND);
		} else {
			responseEntity = new ResponseEntity<>(orders, HttpStatus.OK);
			return responseEntity;
		}

	}

	/**
	 * This method gets the orders from database by using customer Id and returns
	 * the order details.
	 * 
	 * @param customerID
	 * @return
	 */
	public ResponseEntity<?> getOrder(String customerID) {

		ResponseEntity<Object> responseEntity = null;

		CustomerDetails customerDetails = customerService.getCustomerDetails(customerID);

		if (customerDetails == null) {
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		} else {
			List<Orders> order = orderRepository.findOrdersById(customerID);
			if (order.size() <= 0) {
				throw new OrderDetailException(NO_RECORDS_FOUND);
			} else {
				responseEntity = new ResponseEntity<>(order, HttpStatus.OK);
				return responseEntity;
			}
		}

	}

	/**
	 * This method gets the recent orders from database by using customer Id and
	 * returns the order details.
	 * 
	 * @param customerID
	 * @return
	 */
	public ResponseEntity<?> getRecentOrderById(String customerID) {

		ResponseEntity<Object> responseEntity = null;

		List<Orders> order = orderRepository.findRecentOrdersById(customerID);
		responseEntity = new ResponseEntity<>(order, HttpStatus.OK);
		return responseEntity;

	}

	/**
	 * This method gets the last 2 months orders from database.
	 * 
	 * @return
	 */
	public ResponseEntity<?> getRecentOrders() {

		ResponseEntity<Object> responseEntity = null;

		List<Orders> order = orderRepository.findRecentOrders();
		responseEntity = new ResponseEntity<>(order, HttpStatus.OK);
		return responseEntity;

	}

	/**
	 * This methods returns the order stats for the admin
	 * 
	 * @return responseEntity of total stats.
	 */

	public ResponseEntity<?> getTotalOrdersCount() {

		ResponseEntity<Object> responseEntity = null;

		Object totalOrdersCount = orderRepository.findTotalOrdersCount("CREATED");

		Object[] obj = (Object[]) totalOrdersCount;
		int createdCount = 0;
		int cancelledCount = 0;
		int acceptedCount = 0;
		int deliveredCount = 0;
		if (obj[0] != null) {
			createdCount = Integer.valueOf(obj[0].toString());

		}
		if (obj[1] != null) {
			cancelledCount = Integer.valueOf(obj[1].toString());

		}
		if (obj[2] != null) {
			acceptedCount = Integer.valueOf(obj[2].toString());

		}
		if (obj[3] != null) {
			deliveredCount = Integer.valueOf(obj[3].toString());

		}

		int totalOrderCount = createdCount + cancelledCount + acceptedCount + deliveredCount;
		List<OrderStatstics> orderStats = orderStasticsRepository.findAllOrderStastics();

		for (OrderStatstics order : orderStats) {
			order.setTotalOrderCount(totalOrderCount);
			if (order.getStatus().equals(OrderStatus.CREATED.toString())) {
				order.setTotalCount(createdCount);
			}
			if (order.getStatus().equals(OrderStatus.ACCEPTED.toString())) {
				order.setTotalCount(acceptedCount);
			}
			if (order.getStatus().equals(OrderStatus.DELIVERED.toString())) {
				order.setTotalCount(deliveredCount);
			}
			if (order.getStatus().equals(OrderStatus.CANCELLED.toString())) {
				order.setTotalCount(cancelledCount);
			}
		}

		responseEntity = new ResponseEntity<>(orderStats, HttpStatus.OK);
		return responseEntity;

	}

	/**
	 * This methods deletes the specified order id from database.
	 * 
	 * @return ResponseEntity
	 */

	public ResponseEntity<?> deleteByOrderId(String id) {
		ResponseEntity<Object> responseEntity = null;

		if (fetchOrderByOrderId(id) == null) {

			throw new OrderDetailException(NO_ORDER_EXIST);
		}
		orderRepository.deleteById(id);

		if (fetchOrderByOrderId(id) == null) {
			ProductDetails.Response response = new ProductDetails().new Response();
			response.setCode(200);
			response.setMessage(ORDER_DELETED_MSG);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}

		return responseEntity;
	}

	/**
	 * This methods deletes the specified order id from database.
	 * 
	 * @return ResponseEntity
	 */

	public ResponseEntity<?> deleteBySubOrderId(String id) {
		ResponseEntity<Object> responseEntity = null;

		if (fetchOrderBySubOrderId(id) == null) {

			throw new OrderDetailException(NO_ORDER_EXIST);
		}
		orderDetailsRepository.deleteById(id);

		if (fetchOrderBySubOrderId(id) == null) {
			ProductDetails.Response response = new ProductDetails().new Response();
			response.setCode(200);
			response.setMessage(ORDER_DELETED_MSG);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}

		return responseEntity;

	}

	public ResponseEntity<?> createOrders(Orders newOrders) {

		ResponseEntity<Object> responseEntity = null;
		CustomerDetails customerDetails;
		if (newOrders.getOrderDetails() == null || newOrders.getOrderDetails().size() == 0) {
			throw new OrderDetailException("Orders Details are empty");

		}
		if (newOrders.getCustomerDetails().getCustomerId() == null) {
			throw new CustomerDetailException(CUSTOMER_ID_REQUIRED);
		}
		customerDetails = customerService.getCustomerDetails(newOrders.getCustomerDetails().getCustomerId());
		if (customerDetails == null) {
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		}
		String role = customerDetails.getRole();
		if (newOrders.getAddress() == null || newOrders.getAddress().equals("")) {
			throw new OrderDetailException("Shipping Address is required");
		}
		String idValue = commonUtils.getRandomNumberString();
		newOrders.setId(ORDER_ID_PREFIX + idValue);
		newOrders.setCreatedDate(commonUtils.getDate());
		newOrders.setComments(ORDER_CREATED);
		newOrders.getOrderDetails().forEach(orderDetail -> {
			if (orderDetail.getQuantity().equals("") || orderDetail.getQuantity() == null) {
				throw new OrderDetailException("Quantity is required");
			} else if (orderDetail.getProductId().equals("") || orderDetail.getProductId() == null) {
				throw new OrderDetailException("ProductId is required");
			}
			ProductDetails productDetail = productService.fetchByProductId(orderDetail.getProductId());
			if (productDetail == null) {
				throw new ProductDetailException(PRODUCT_NOT_AVAILABLE);
			}
			orderDetail.setOrderDetailId(ORDER_DETAILID_PREFIX + commonUtils.getRandomNumberString());
			orderDetail.setOrders(newOrders);
			orderDetail.setCreatedDate(commonUtils.getDate());
			orderDetail.setUpdatedDate("");
			orderDetail.setCreatedBy(role);
			orderDetail.setUpdatedBy("");

		});
		orderRepository.save(newOrders);
		Orders insertedOrders = orderRepository.findById(newOrders.getId()).get();
		if (insertedOrders != null) {
			sendMail(newOrders);
			OrderDetails.Response response = new OrderDetails().new Response();
			response.setCode(200);
			response.setMessage(ORDER_CREATED_MSG);
			response.setStatus(SUCCESS);
			response.setData(insertedOrders);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);

		}

		return responseEntity;

	}

	@Async
	public void sendMail(Orders newOrders) {
		emailServiceImpl.sendBookingConfirmationMailForAdmin(newOrders);
		emailServiceImpl.sendBookingConfirmationMailForCustomer(newOrders);
		cartService.deleteAllCart(newOrders.getCustomerDetails().getCustomerId());
	}

	/**
	 * This methods updates the product to database.
	 * 
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> updateOrder(Orders updateOrder) {
		ResponseEntity<Object> responseEntity = null;
		if (updateOrder.getId() == null) {
			throw new OrderDetailException(ORDER_ID_REQUIRED);
		}

		Orders existingOrder = fetchOrderByOrderId(updateOrder.getId());
		if (existingOrder == null) {

			throw new OrderDetailException(NO_ORDER_EXIST);
		}

		if (updateOrder.getComments() != null) {
			if (!updateOrder.getComments().equals("")) {
				existingOrder.setComments(updateOrder.getComments());
			}
		}
		if (updateOrder.getOrderAmount() != null) {
			if (!updateOrder.getOrderAmount().equals("")) {
				existingOrder.setOrderAmount(updateOrder.getOrderAmount());
			}
		}
		if (updateOrder.getTotalOrderAmount() != null) {
			if (!updateOrder.getTotalOrderAmount().equals("")) {
				existingOrder.setTotalOrderAmount(updateOrder.getTotalOrderAmount());

			}
		}
		if (updateOrder.getOrderDetails() != null) {
			for (int i = 0; i < updateOrder.getOrderDetails().size(); i++) {

				updateOrder.getOrderDetails().get(i).setOrders(existingOrder);
			}
			existingOrder.setOrderDetails(updateOrder.getOrderDetails());
		}
		existingOrder.setUpdatedDate(commonUtils.getDate());
		existingOrder = validateOrderStatus(updateOrder, existingOrder);
		orderRepository.save(existingOrder);
		Orders updatedOrders = orderRepository.findById(updateOrder.getId()).get();

		if (updatedOrders != null) {
			OrderDetails.Response response = new OrderDetails().new Response();
			response.setCode(200);
			response.setMessage(ORDER_UPDATED_MSG);
			response.setStatus(SUCCESS);
			response.setData(updatedOrders);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}

		return responseEntity;

	}

	/**
	 * This methods gets the orders from order details database by orderId and
	 * returns the order details.
	 * 
	 * @return Order details
	 */

	public OrderDetails fetchOrderBySubOrderId(String id) {
		Optional<OrderDetails> orderDetails = orderDetailsRepository.findById(id);

		if (orderDetails.isPresent()) {
			return orderDetails.get();
		} else {
			return null;
		}

	}

	/**
	 * This methods gets the orders from order database by orderId and returns the
	 * orders.
	 * 
	 * @return Orders
	 */

	public Orders fetchOrderByOrderId(String id) {
		Optional<Orders> orders = orderRepository.findById(id);

		if (orders.isPresent()) {
			return orders.get();
		} else {
			return null;
		}

	}

	public Orders validateOrderStatus(Orders updateOrder, Orders existingOrder) {

		if (updateOrder.getComments() != null) {

			if (updateOrder.getComments().equals(OrderStatus.CREATED.toString())) {
				existingOrder.setCreatedDate(updateOrder.getSelectedDate());
			} else if (updateOrder.getComments().equals(OrderStatus.ACCEPTED.toString())) {
				existingOrder.setAcceptedDate(updateOrder.getSelectedDate());
			} else if (updateOrder.getComments().equals(OrderStatus.DELIVERED.toString())) {
				existingOrder.setDeliveryDate(updateOrder.getSelectedDate());
			} else if (updateOrder.getComments().equals(OrderStatus.CANCELLED.toString())) {
				existingOrder.setCancelledDate(updateOrder.getSelectedDate());
			}
		}

		return existingOrder;
	}

}
