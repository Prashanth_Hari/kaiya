package com.kaiya.service;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.multipart.MultipartFile;

import com.kaiya.entity.AdminDetails;
import com.kaiya.entity.CustomerDetails;
import com.kaiya.entity.Orders;

import static com.kaiya.api.Constants.NO_REPLY;
import static com.kaiya.api.Constants.KAIYA_BOOKING;
import static com.kaiya.api.Constants.SUPPORT_EMAIL;
import static com.kaiya.api.Constants.FORGOT_PASSWORD;
import static com.kaiya.api.Constants.KAIYA;
import static com.kaiya.api.Constants.INVOICE_SUBJECT;

import java.io.StringWriter;

import static com.kaiya.api.Constants.ADMIN_SUBJECT;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

@Component
public class EmailServiceImpl {

	private static Logger LOGGER = LogManager.getLogger(EmailServiceImpl.class);

	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	private Configuration freemarkerConfig;

	public void sendBookingConfirmationMailForAdmin(Orders orders) {
		LOGGER.info("Inside sendBookingConfirmationMailForAdmin");
		try {
			MimeMessage message = emailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(message);
			freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");
			Template t = freemarkerConfig.getTemplate("BookingConfirmationAdmin.ftl");
			String text = FreeMarkerTemplateUtils.processTemplateIntoString(t, orders);

			helper.setFrom(new InternetAddress(NO_REPLY, KAIYA_BOOKING));
			helper.setTo(SUPPORT_EMAIL);
			helper.setText(text, true);
			helper.setSubject(ADMIN_SUBJECT + orders.getId());

			emailSender.send(message);
		} catch (Exception e) {
			LOGGER.info("Email Exception");
			LOGGER.info(e.getMessage());
			e.printStackTrace();
		}
		LOGGER.info("End sendBookingConfirmationMailForAdmin");
	}

	public void sendBookingConfirmationMailForCustomer(Orders orders) {
		LOGGER.info("Start sendBookingConfirmationMailForCustomer");
		try {
			MimeMessage message = emailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(message);
			StringWriter out = new StringWriter();

			Configuration cfg = new Configuration();
			cfg.setClassForTemplateLoading(this.getClass(), "/templates");
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			Template temp = cfg.getTemplate("BookingConfirmationCustomer.ftl");
			String text = FreeMarkerTemplateUtils.processTemplateIntoString(temp, orders);
			helper.setFrom(new InternetAddress(NO_REPLY, KAIYA_BOOKING));
			helper.setTo(orders.getAddress().getEmailId());
			helper.setText(text, true);
			helper.setSubject(ADMIN_SUBJECT + orders.getId());

			emailSender.send(message);
		} catch (Exception e) {
			LOGGER.info("Email Exception");
			LOGGER.info(e.getMessage());
			e.printStackTrace();
		}
		LOGGER.info("End sendBookingConfirmationMailForCustomer");
	}

	public void sendInvoice(Orders orders, String fileName) {
		LOGGER.info("Start sendInvoice");
		try {
			MimeMessage message = emailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			StringWriter out = new StringWriter();

			Configuration cfg = new Configuration();
			cfg.setClassForTemplateLoading(this.getClass(), "/templates");
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			Template temp = cfg.getTemplate("CustomerInvoice.ftl");
			String text = FreeMarkerTemplateUtils.processTemplateIntoString(temp, orders);
			helper.setFrom(new InternetAddress(NO_REPLY, KAIYA_BOOKING));
			helper.setTo(orders.getAddress().getEmailId());
			helper.setText(text, true);
			FileSystemResource getCreatedFile = new FileSystemResource("./Images/" + fileName);
			helper.addAttachment(getCreatedFile.getFilename(), getCreatedFile);
			helper.setSubject(INVOICE_SUBJECT + orders.getId());

			emailSender.send(message);
		} catch (Exception e) {
			LOGGER.info("Invoice mail Exception");
			LOGGER.info(e.getMessage());
			e.printStackTrace();
		}
		LOGGER.info("Invoice Sent");
	}

	/**
	 * This method is used to send the otp to customers for resetting the password.
	 * 
	 * @param customerDetails
	 */
	public void sendForgotPasswordLink(CustomerDetails customerDetails, AdminDetails adminDetails) {
		LOGGER.info("Start sendForgotPasswordLink");
		try {
			MimeMessage message = emailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(message);
			StringWriter out = new StringWriter();

			Configuration cfg = new Configuration();
			cfg.setClassForTemplateLoading(this.getClass(), "/templates");
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			Template temp = cfg.getTemplate("ForgotPasswordLink.ftl");
			if (customerDetails != null) {
				String text = FreeMarkerTemplateUtils.processTemplateIntoString(temp, customerDetails);
				helper.setTo(customerDetails.getEmailId());
				helper.setText(text, true);
			} else if (adminDetails != null) {
				String text = FreeMarkerTemplateUtils.processTemplateIntoString(temp, adminDetails);
				helper.setTo(adminDetails.getEmailId());
				helper.setText(text, true);
			}
			helper.setFrom(new InternetAddress(NO_REPLY, KAIYA));

			helper.setSubject(FORGOT_PASSWORD);
			emailSender.send(message);
		} catch (Exception e) {
			LOGGER.info("Email Exception");
			LOGGER.info(e.getMessage());
			e.printStackTrace();
		}
		LOGGER.info("End sendForgotPasswordLink");
	}

}
