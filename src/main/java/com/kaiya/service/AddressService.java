package com.kaiya.service;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.kaiya.dao.AddressDetailsRepository;
import com.kaiya.entity.Address;
import com.kaiya.exceptions.CustomerDetailException;
import com.kaiya.util.CommonUtils;

import static com.kaiya.api.Constants.CUSTOMER_NOT_FOUND;
import static com.kaiya.api.Constants.ADDRESS_CREATED_MSG;
import static com.kaiya.api.Constants.SUCCESS;
import static com.kaiya.api.Constants.ADDRESS_DELETED_MSG;
import static com.kaiya.api.Constants.ADDRESS_UPDATED_MSG;
import static com.kaiya.api.Constants.ADDRESS_ID_REQUIRED;
import static com.kaiya.api.Constants.ADDRESS_NOT_FOUND;
import static com.kaiya.api.Constants.NO_RECORDS_CHANGED;
import static com.kaiya.api.Constants.CUSTOMER_ID_MSG;

@Service
public class AddressService {

	private static Logger LOGGER = LogManager.getLogger(AddressService.class);

	@Autowired
	private AddressDetailsRepository addressDetailsRepository;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private CommonUtils commonUtils;

	/**
	 * This method is used to save the address to the database.
	 * 
	 * @param address
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> saveAddress(Address address) {
		LOGGER.info("Inside saveAddress");
		ResponseEntity<Object> responseEntity = null;
		Address returnedAddress = null;
		try {
			if (address.getAddress() == null || address.getAddress().equals("")) {
				LOGGER.info("Address is empty so throwing exception");
				throw new CustomerDetailException("Address is required");
			}

			address.setId("ADD" + commonUtils.getRandomNumberString());
			address.setCreatedDate(commonUtils.getDate());
			LOGGER.info("Before Save address");
			returnedAddress = addressDetailsRepository.save(address);

		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Inside create address" + e);
			throw new CustomerDetailException("Address already exist");
		}
		if (returnedAddress.getId() != null) {
			LOGGER.info("End Save address");
			Address.Response response = new Address().new Response();
			response.setCode(200);
			response.setMessage(ADDRESS_CREATED_MSG);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End Save address");
		return responseEntity;

	}

	/**
	 * This method is used to get the address details for the given address id.
	 * 
	 * @param addressId
	 * @return Address
	 */
	public Address getAddressDetails(String addressId) {
		LOGGER.info("Inside  of getAddressDetails");
		if (addressId == null) {
			LOGGER.info("Address ID is Null");
			throw new CustomerDetailException(ADDRESS_ID_REQUIRED);
		}
		Optional<Address> address = addressDetailsRepository.findById(addressId);
		if (address.isPresent()) {
			LOGGER.info("Address is Present");
			LOGGER.info("End of getAddressDetails");
			return address.get();
		} else {
			LOGGER.info("Address is empty");
			LOGGER.info("End of getAddressDetails");
			return null;
		}

	}

	/**
	 * This method is used to get the address details for the given customer id.
	 * 
	 * @param customerId
	 * @return Address
	 */
	public ResponseEntity<?> getAddressDetailsByCustomerId(String customerId) {
		ResponseEntity<Object> responseEntity = null;
		LOGGER.info("Inside of getAddressDetailsByCustomerId service");
		if (customerId == null) {
			LOGGER.info("customerId is null so throwing exception");
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		}
		List<Address> address = addressDetailsRepository.findAddressByCustomerId(customerId);
		Optional<Address> defaultAddress = addressDetailsRepository.findDefaultAddress(customerId, "true");

		Address.Response response = new Address().new Response();
		response.setCode(200);
		response.setStatus(SUCCESS);
		response.setAddressDetails(address);
		if (defaultAddress.isPresent()) {
			LOGGER.info("defaultAddress is Present");
			response.setDefaultAddress(defaultAddress.get());

		} else {
			LOGGER.info("defaultAddress is empty");
			response.setDefaultAddress(null);

		}
		responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		LOGGER.info("End of getAddressDetailsByCustomerId");
		return responseEntity;

	}

	/**
	 * This method is used to get the default address details for the given customer
	 * id.
	 * 
	 * @param customerId
	 * @return Address
	 */
	public ResponseEntity<?> getDefaultAddress(String customerId) {
		ResponseEntity<Object> responseEntity = null;
		LOGGER.info("Inside of getDefaultAddress service");
		if (customerId == null) {
			LOGGER.info("customerId is null so throwing exception");
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		}
		Optional<Address> address = addressDetailsRepository.findDefaultAddress(customerId, "true");

		Address.Response response = new Address().new Response();
		response.setCode(200);
		response.setStatus(SUCCESS);
		if (address.isPresent()) {
			LOGGER.info("address is Present");
			response.setDefaultAddress(address.get());

		} else {
			LOGGER.info("address is empty");
			response.setDefaultAddress(null);

		}
		responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		LOGGER.info("End of getDefaultAddress");
		return responseEntity;

	}

	/**
	 * This methods deletes the address from database.
	 * 
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> deleteAddress(String addressId) {
		LOGGER.info("Inside of deleteAddress service");
		ResponseEntity<Object> responseEntity = null;

		if (addressId == null || addressId.equals("")) {
			LOGGER.info("addressId is null so throwing exception");
			throw new CustomerDetailException(ADDRESS_ID_REQUIRED);
		}
		if (getAddressDetails(addressId) == null) {

			throw new CustomerDetailException(ADDRESS_NOT_FOUND);
		}
		addressDetailsRepository.deleteById(addressId);

		if (getAddressDetails(addressId) == null) {
			Address.Response response = new Address().new Response();
			response.setCode(200);
			response.setMessage(ADDRESS_DELETED_MSG);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End of deleteAddress");
		return responseEntity;

	}

	/**
	 * This method is used to get the all address details for the database.
	 * 
	 * @return Iterable <Address>
	 */
	public Iterable getAllAddress() {

		return addressDetailsRepository.findAll();

	}

	/**
	 * This method is used to update the address to database.
	 * 
	 * @param updateAddress
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> updateAddress(Address updateAddress) {
		LOGGER.info("Inside of updateAddress service");
		ResponseEntity<Object> responseEntity = null;
		Address updatedAddress = null;

		if (updateAddress.getId() == null) {
			LOGGER.info("addressId is null so throwing exception");
			throw new CustomerDetailException(ADDRESS_ID_REQUIRED);
		}
		if (updateAddress.getCustomerDetails() == null || updateAddress.getCustomerDetails().getCustomerId() == null) {
			LOGGER.info("Customer Details or  Customer ID is null so throwing exception");
			throw new CustomerDetailException(CUSTOMER_ID_MSG);
		}
		if (customerService.getCustomerDetails(updateAddress.getCustomerDetails().getCustomerId()) == null) {
			LOGGER.info("Customer Details is null so throwing exception");
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		}

		Address existingAddress = getAddressDetails(updateAddress.getId());
		if (existingAddress == null) {
			LOGGER.info("existing Customer Address is null so throwing exception");
			throw new CustomerDetailException(ADDRESS_NOT_FOUND);
		}
		if (existingAddress.getAddress().equals(updateAddress.getAddress())
				&& existingAddress.getAddressType().equals(updateAddress.getAddressType())
				&& existingAddress.getCity().equals(updateAddress.getCity())
				&& existingAddress.getState().equals(updateAddress.getState())
				&& existingAddress.getName().equals(updateAddress.getName())
				&& existingAddress.getEmailId().equals(updateAddress.getEmailId())
				&& existingAddress.getPhoneNumber().equals(updateAddress.getPhoneNumber())
				&& existingAddress.getPincode().equals(updateAddress.getPincode())) {
			LOGGER.info("No Records changed exception");
			throw new CustomerDetailException(NO_RECORDS_CHANGED);
		}

		if (!(updateAddress.getAddress() == null || updateAddress.getAddress().equals(""))) {
			existingAddress.setAddress(updateAddress.getAddress());
		}
		if (!(updateAddress.getAddressDefaultValue() == null || updateAddress.getAddressDefaultValue().equals(""))) {
			existingAddress.setAddressDefaultValue(updateAddress.getAddressDefaultValue());
		}
		if (!(updateAddress.getCity() == null || updateAddress.getCity().equals(""))) {
			existingAddress.setCity(updateAddress.getCity());
		}
		if (!(updateAddress.getState() == null || updateAddress.getState().equals(""))) {
			existingAddress.setState(updateAddress.getState());
		}
		if (!(updateAddress.getName() == null || updateAddress.getName().equals(""))) {
			existingAddress.setName(updateAddress.getName());
		}
		if (!(updateAddress.getPhoneNumber() == null || updateAddress.getPhoneNumber().equals(""))) {
			existingAddress.setPhoneNumber(updateAddress.getPhoneNumber());
		}
		if (!(updateAddress.getPincode() == null || updateAddress.getPincode().equals(""))) {
			existingAddress.setPincode(updateAddress.getPincode());
		}
		if (!(updateAddress.getAddressType() == null || updateAddress.getAddressType().equals(""))) {
			existingAddress.setAddressType(updateAddress.getAddressType());
		}
		if (!(updateAddress.getEmailId() == null || updateAddress.getEmailId().equals(""))) {
			existingAddress.setEmailId(updateAddress.getEmailId());
		}
		updatedAddress = addressDetailsRepository.save(existingAddress);

		if (updatedAddress.getId() != null) {
			Address.Response response = new Address().new Response();
			response.setCode(200);
			response.setMessage(ADDRESS_UPDATED_MSG);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		return responseEntity;

	}

	/**
	 * This method is used to update the default address value to database.
	 * 
	 * @param updateAddress
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> updateDefaultAddress(Address updateAddress) {
		LOGGER.info("Inside of updateDefaultAddress");
		ResponseEntity<Object> responseEntity = null;
		Address updatedAddress = null;

		if (updateAddress.getId() == null)
			throw new CustomerDetailException(ADDRESS_ID_REQUIRED);
		if (updateAddress.getCustomerDetails() == null || updateAddress.getCustomerDetails().getCustomerId() == null) {
			throw new CustomerDetailException(CUSTOMER_ID_MSG);
		}
		if (customerService.getCustomerDetails(updateAddress.getCustomerDetails().getCustomerId()) == null) {
			throw new CustomerDetailException(CUSTOMER_NOT_FOUND);
		}
		Address existingAddress = getAddressDetails(updateAddress.getId());
		if (existingAddress == null)
			throw new CustomerDetailException(ADDRESS_NOT_FOUND);

		if (!(updateAddress.getAddressDefaultValue() == null || updateAddress.getAddressDefaultValue().equals(""))) {
			existingAddress.setAddressDefaultValue(updateAddress.getAddressDefaultValue());
		}
		updatedAddress = addressDetailsRepository.save(existingAddress);

		if (updatedAddress.getId() != null) {
			Address.Response response = new Address().new Response();
			response.setCode(200);
			response.setMessage(ADDRESS_UPDATED_MSG);
			response.setStatus(SUCCESS);
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End of updateDefaultAddress");
		return responseEntity;

	}

}
