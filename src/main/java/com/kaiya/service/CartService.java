package com.kaiya.service;

import static com.kaiya.api.Constants.CART_ADDED_MSG;
import static com.kaiya.api.Constants.CART_DELETED_MSG;
import static com.kaiya.api.Constants.SUCCESS;
import static com.kaiya.api.Constants.CART_UPDATED_MSG;
import static com.kaiya.api.Constants.AVAILABLE;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.kaiya.dao.CartDetailsRepository;
import com.kaiya.entity.CartDetails;
import com.kaiya.exceptions.ProductDetailException;
import com.kaiya.util.CommonUtils;

@Service
public class CartService {
	private static Logger LOGGER = LogManager.getLogger(CartService.class);
	@Autowired
	private CartDetailsRepository cartDetailsRepository;

	@Autowired
	private CommonUtils commonUtils;

	/**
	 * This methods gets the cart details from database and returns the list of cart
	 * details.
	 * 
	 * @return Array of cart details
	 */
	@SuppressWarnings("unchecked")
	public ResponseEntity<?> getCartDetails(String customerId) {
		LOGGER.info("Inside of getCartDetails");
		ResponseEntity<?> responseEntity = null;

		List<CartDetails> cartDetails = cartDetailsRepository.findByCustomerId(customerId);
		Integer totalCartAmount = cartDetailsRepository.findCartAmount(customerId, AVAILABLE);
		if (cartDetails != null) {
			CartDetails.Response response = new CartDetails().new Response();
			response.setCode(200);
			response.setMessage(CART_ADDED_MSG);
			response.setStatus(SUCCESS);
			response.setTotalNumberOfItem(cartDetails.size());
			response.setCartDetails(cartDetails);
			if (totalCartAmount == null) {
				response.setTotalCartAmount(0);
			} else {
				response.setTotalCartAmount(totalCartAmount.intValue());
			}
			responseEntity = new ResponseEntity(response, HttpStatus.OK);
		}
		LOGGER.info("End of getCartDetails");
		return responseEntity;
	}

	/**
	 * This methods adds the cart details to database.
	 * 
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> addToCart(CartDetails cartDetails) {
		LOGGER.info("Inside of addToCart");
		ResponseEntity<Object> responseEntity = null;

		CartDetails addedCartDetails = null;

		List<CartDetails> totalItems = null;

		cartDetails.setCartId("CRT" + commonUtils.getRandomNumberString());

		cartDetails.setCreatedDate(commonUtils.getDate());

		try {
			addedCartDetails = cartDetailsRepository.save(cartDetails);
			totalItems = getCartDetailsByCustomerd(cartDetails.getCustomerId());
		} catch (Exception exception) {
			throw new ProductDetailException(exception.getMessage());
		}

		if (addedCartDetails != null) {
			CartDetails.Response response = new CartDetails().new Response();
			response.setCode(200);
			response.setMessage(CART_ADDED_MSG);
			response.setStatus(SUCCESS);
			response.setTotalNumberOfItem(totalItems.size());
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End of addToCart");
		return responseEntity;

	}

	/**
	 * This methods adds the cart details to database.
	 * 
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> updateCart(CartDetails cartDetails) {
		LOGGER.info("Inside of updateCart");
		ResponseEntity<Object> responseEntity = null;

		CartDetails updatedCartDetails = null;

		List<CartDetails> totalItems = null;
		CartDetails existingCartDetails = getCartDetailsByCartId(cartDetails.getCartId());

		existingCartDetails.setQuantity(cartDetails.getQuantity());
		existingCartDetails.setTotalAmount(cartDetails.getTotalAmount());
		try {
			updatedCartDetails = cartDetailsRepository.save(existingCartDetails);
			totalItems = getCartDetailsByCustomerd(cartDetails.getCustomerId());
		} catch (Exception exception) {
			LOGGER.info("Product Detail Exception");
			throw new ProductDetailException(exception.getMessage());
		}

		if (updatedCartDetails != null) {
			CartDetails.Response response = new CartDetails().new Response();
			response.setCode(200);
			response.setMessage(CART_UPDATED_MSG);
			response.setStatus(SUCCESS);
			response.setTotalNumberOfItem(totalItems.size());
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End of updateCart");
		return responseEntity;

	}

	public void deleteAllCart(String customerId) {
		LOGGER.info("Inside of deleteAllCart");
		cartDetailsRepository.deleteCartByCustomerId(customerId);
		LOGGER.info("End of deleteAllCart");
	}

	public ResponseEntity<?> deleteCart(String cartId, String customerId) {
		LOGGER.info("Inside of deleteCart");
		ResponseEntity<Object> responseEntity = null;
		cartDetailsRepository.deleteById(cartId);
		List<CartDetails> totalItems = getCartDetailsByCustomerd(customerId);
		if (getCartDetailsByCartId(cartId) == null) {
			CartDetails.Response response = new CartDetails().new Response();
			response.setCode(200);
			response.setMessage(CART_DELETED_MSG);
			response.setStatus(SUCCESS);
			if (totalItems == null) {
				response.setTotalNumberOfItem(0);
			} else if (totalItems != null) {
				response.setTotalNumberOfItem(totalItems.size());

			}
			responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.info("End of deleteCart");
		return responseEntity;

	}

	/**
	 * This methods will deletes the cart details from database only if the product
	 * is deleted by admin.
	 * 
	 */
	public void deleteCartDetailsByProductId(String productId) {
		LOGGER.info("Inside of getCartDetailsByCartId");
		Optional<CartDetails> cartDetails = cartDetailsRepository.findByProductId(productId);

		if (cartDetails.isPresent()) {
			LOGGER.info("End of getCartDetailsByProductId");

			cartDetailsRepository.deleteCartByProductId(productId);

		} else {
			LOGGER.info("End of getCartDetailsByProductId");

		}
	}

	/**
	 * This methods gets the cart details from database and returns the list of cart
	 * details.
	 * 
	 * @return Array of cart details
	 */
	public CartDetails getCartDetailsByCartId(String cartId) {
		LOGGER.info("Inside of getCartDetailsByCartId");
		Optional<CartDetails> cartDetails = cartDetailsRepository.findById(cartId);

		if (cartDetails.isPresent()) {
			LOGGER.info("End of getCartDetailsByCartId");
			return cartDetails.get();
		} else {
			LOGGER.info("End of getCartDetailsByCartId");
			return null;

		}
	}

	/**
	 * This methods gets the cart details from database and returns the list of cart
	 * details.
	 * 
	 * @return Array of cart details
	 */
	public List<CartDetails> getCartDetailsByCustomerd(String customerId) {
		LOGGER.info("Inside of getCartDetailsByCustomerd");
		List<CartDetails> cartDetails = cartDetailsRepository.findByCustomerId(customerId);

		if (cartDetails.isEmpty()) {
			LOGGER.info("End of getCartDetailsByCustomerd");
			return null;
		} else {
			LOGGER.info("End of getCartDetailsByCustomerd");
			return cartDetails;

		}
	}

}
