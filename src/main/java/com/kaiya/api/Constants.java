package com.kaiya.api;

/**
 * This class is used to hold all constants.
 *
 */
public class Constants {

	public static final String CUSTOMER_ROLE = "CUSTOMER";

	public static final String AVAILABLE = "Available";

	public static final String ADMIN_ROLE = "ADMIN";

	public static final String LOGIN = "/login";

	public static final String DELETE_CART = "/deleteCart";

	public static final String GET_CART = "/getCart";

	public static final String ADD_CART = "/addCart";

	public static final String UPDATE_CART = "/updateCart";

	public static final String ADD_PRODUCT = "/addProduct";

	public static final String UPDATE_PRODUCT = "/updateProduct";

	public static final String GET_PRODUCTS = "/getProducts";

	public static final String GET_ALL_PRODUCTS = "/getAllProducts";

	public static final String GET_ALL_INVOICE = "/getAllInvoice";

	public static final String GET_INVOICE_BY_CUSTOMER_ID = "/getInvoiceByCustomerId";

	public static final String GET_INVOICE_BY_INVOICE_ID = "/getInvoiceByInvoiceId";

	public static final String CREATE_INVOICE = "/createInvoice";

	public static final String UPDATE_INVOICE = "/updateInvoice";

	public static final String GET_ORDERS = "/getOrders";

	public static final String GET_ORDERS_BY_ORDERID = "/getOrdersByOrderId";

	public static final String GET_ORDERS_BY_PHONE_NUMBER = "/getOrdersByPhoneNumber";

	public static final String GET_ORDERS_BY_EMAILID = "/getOrdersByEmailId";

	public static final String GET_ORDERS_BY_DATE = "/getOrdersByDate";

	public static final String GET_ORDERS_BY_DATE_RANGE = "/getOrdersByDateRange";

	public static final String GET_RECENT_ORDERS = "/getRecentOrders";

	public static final String GET_TOTAL_ORDERS_COUNT = "/getTotalOrdersCount";

	public static final String CREATE_ORDER = "/createOrder";

	public static final String UPDATE_ORDER = "/updateOrder";

	public static final String CANCEL_ORDER = "/cancelOrder";

	public static final String DELETE_ORDER = "/deleteOrder";

	public static final String DELETE_SUB_ORDER = "/deleteSubOrder";

	public static final String DELETE_PRODUCT = "/deleteProduct";

	public static final String CREATE_CUSTOMER = "/createCustomer";

	public static final String GET_CUSTOMER = "/getCustomer";

	public static final String GET_ALL_CUSTOMER = "/getAllCustomer";

	public static final String GET_CUSTOMER_DETAILS_BY_ID = "/getCustomerById";

	public static final String GET_ADDRESS_BY_CUSTOMER_ID = "/getAddressByCustId";

	public static final String GET_DEFAULT_ADDRESS = "/getDefaultAddress";

	public static final String GET_ADDRESS_BY_ID = "/getAddressById";

	public static final String GET_ALL_ADDRESS = "/getAllAddress";

	public static final String CREATE_ADDRESS = "/createAddress";

	public static final String DELETE_ADDRESS = "/deleteAddress";

	public static final String GET_ADMIN = "/getAdmin";

	public static final String GET_ALL_ADMIN = "/getAllAdminDetails";

	public static final String GET_ADMIN_BY_USERID = "/getAdminByUserId";

	public static final String CHANGE_PASSWORD = "/changePassword";

	public static final String CHANGE_PASSWORD_ADMIN = "/changePasswordAdmin";

	public static final String GET_CUSTOMER_CARE_DETAILS = "/getCustomerCareDetails";

	public static final String RESET_PASSWORD_EMAIL_LINK = "/resetPasswordLink";

	public static final String RESET_PASSWORD_EMAIL_LINK_ADMIN = "/resetPasswordLinkAdmin";

	public static final String RESET_PASSWORD = "/resetPassword";

	public static final String RESET_PASSWORD_ADMIN = "/resetPasswordAdmin";

	public static final String UPDATE_CUSTOMER = "/updateCustomer";

	public static final String UPDATE_ADDRESS = "/updateAddress";

	public static final String UPDATE_DEFAULT_ADDRESS = "/updateDefaultAddress";

	public static final String ADD_ADMIN = "/addAdmin";

	public static final String UPDATE_ADMIN = "/updateAdmin";

	public static final String DELETE_ADMIN = "/deleteAdmin";

	public static final String ONEWAY = "OnewayTrip";

	public static final String ROUND_TRIP = "RoundTrip";

	public static final String LOCAL_PACKAGE = "LocalPackage";

	public static final String POST_QUERY = "/postQuery";

	public static final String EMAIL_ALREADY_EXIST = "Email Id Already exist!";

	public static final String MOBILENUMBER_ALREADY_EXIST = "Phone Number Already exist!";

	public static final String GST_ALREADY_EXIST = "GST_ID Already exist!";

	public static final String USERNAME_PASSWORD_ERR_MSG = "Username or Password wrong!";

	public static final String PRODUCT_NOT_AVAILABLE = "Product is not available!";

	public static final String PRODUCT_EXIST_ERR_MSG = "Product Already Exist!";

	public static final String INVOICE_CREATED_MSG = "Invoice Created Successfully!";

	public static final String INVOICE_UPDATED_MSG = "Invoice Updated Successfully!";

	public static final String PRODUCT_ADDED_MSG = "Product Added Successfully!";

	public static final String PRODUCT_UPDATED_MSG = "Product Updated Successfully!";

	public static final String PRODUCT_DELETED_MSG = "Product Deleted Successfully!";

	public static final String CART_DELETED_MSG = "Cart Deleted Successfully!";

	public static final String ADDRESS_DELETED_MSG = "Address Deleted Successfully!";

	public static final String ORDER_CREATED_MSG = "Order Created Successfully!";

	public static final String ORDER_DELETED_MSG = "Order Deleted Successfully!";

	public static final String ORDER_UPDATED_MSG = "Order Updated Successfully!";

	public static final String ADMIN_DELETED_MSG = "Admin Deleted Successfully!";

	public static final String CART_ADDED_MSG = "Successfully added to cart!";

	public static final String CART_UPDATED_MSG = "Item Updated Successfully to cart!";

	public static final String NO_ORDER_EXIST = "No order exist !";

	public static final String NO_RECORDS_CHANGED = "No Records Changed!";

	public static final String CUSTOMER_NOT_FOUND = "Customer Not Found!";

	public static final String ADMIN_NOT_FOUND = "Admin Not Found!";

	public static final String ADDRESS_NOT_FOUND = "Address Not Found!";

	public static final String USER_NOT_FOUND = "User Not Found!";

	public static final String INVOICE_NOT_FOUND = "Invoice Not Found!";

	public static final String ADDRESS_ID_REQUIRED = "Address id is required";

	public static final String INVOICE_ID_ERR_MSG = "Invoice id is required!";

	public static final String INVOICE_ALREADY_EXIST = "Invoice id already exist!";

	public static final String CUSTOMER_ID_MSG = "Customer id is required!";

	public static final String USER_ID_REQUIRED = "User id is required!";

	public static final String PASSWORD_REQUIRED = "Password is required!";

	public static final String NEW_PASSWORD_REQUIRED = "New Password is required!";

	public static final String CONFIRM_PASSWORD_REQUIRED = "Confirm Password is required!";

	public static final String SUCCESS = "Success";

	public static final String PRODUCT_ID_PREFIX = "PT00";

	public static final String ORDER_ID_PREFIX = "ORD00";

	public static final String ORDER_DETAILID_PREFIX = "ORDLD00";

	public static final String ADMIN_CREATED_MSG = "Admin created successfully!";

	public static final String CUSTOMER_CREATED_MSG = "Customer created successfully!";

	public static final String ADDRESS_CREATED_MSG = "Address created successfully!";

	public static final String ADDRESS_UPDATED_MSG = "Address updated successfully!";

	public static final String CUSTOMER_UPDATED_MSG = "Personal Details Updated successfully!";

	public static final String ADMIN_UPDATED_MSG = "Admin Details Updated successfully!";

	public static final String ORDER_CREATED = "CREATED";

	public static final String CREATED_BY_CUSTOMER = "CUSTOMER";

	public static final String UPDATED_BY_CUSTOMER = "CUSTOMER";

	public static final String EMAIL_ID_REQUIRED = "Email Id is required";

	public static final String ORDER_ID_REQUIRED = "Order Id is required";

	public static final String COMMENTS_REQUIRED = "Comments is required";

	public static final String CUSTOMER_ID_REQUIRED = "Customer Id is required";

	public static final String STATUS_NOT_FOUND = "Order Status not found. Please provide one of the values Created, Accepted, Delivered and Cancelled.";

	public static final String CUSTOMER_DETAILS_REQUIRED = "Customer Details are required";

	public static final String ADMIN_DETAILS_REQUIRED = "Admin Details are required";

	public static final String PASSWORD_CHANGED = "Password Changed Successfully";

	public static final String SUPPORT_EMAIL = "Kaiya@kumarsbeverages.com";

	public static final String ADMIN_SUBJECT = "Order Received - Order ID:";

	public static final String INVOICE_SUBJECT = "Invoice From Kaiya:";

	public static final String KAIYA_BOOKING = "Kaiya Bookings";

	public static final String KAIYA = "Kaiya";

	public static final String NO_REPLY = "noreply@kumarsbeverages.com";

	public static final String NO_RECORDS_FOUND = "No Records Found";

	public static final String FORGOT_PASSWORD = " Forgot Password";

	public static final String EMAIL_DOSENT_EXIST = "Email Id does not exist!";

	public static final String OTP_VALID_MSG = "OTP is Valid";

	public static final String OTP_INVALID_MSG = "Invalid OTP";

	public static final String EMAIL_SENT_SUCCESSFULLY = "Email Sent Successfully";

	public static final String VALIDATE_OTP = "/validateOTP";

	public static final String VALIDATE_ADMIN_OTP = "/validateAdminOTP";

	private Constants() {

	}

}
