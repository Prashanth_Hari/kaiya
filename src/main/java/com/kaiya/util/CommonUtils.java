package com.kaiya.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class CommonUtils {

	public String getRandomNumberString() {
		// It will generate 6 digit random Number.
		// from 0 to 999999
		Random rnd = new Random();
		int number = rnd.nextInt(999999);

		// this will convert any number sequence into 6 character.
		return String.format("%06d", number);
	}

	public String getDate() {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
		String formattedDate = myDateObj.format(myFormatObj);

		return formattedDate;
	}
	
	/**
	 * This method is used to generate the 4 digit otp number.
	 * 
	 * @return otp
	 */
	public String generateOTP() {
		return String.valueOf((int) (Math.random() * 9000) + 1000);
	}

}
